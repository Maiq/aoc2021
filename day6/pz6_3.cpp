#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <array>
#include <algorithm>

int main(){
    std::string fileName("in6.txt");
    std::fstream file;
    std::string number;

    std::array<long long unsigned int, 9> states = {0,0,0,0,0,0,0,0,0};
    int days80 = 80;
    int days256 = 256;
    int initDay;
    long long unsigned int sum = 0;

    file.open(fileName);
    while(!file.eof()){
        std::getline(file,number,',');
        initDay = std::stoi(number);
        states[initDay] += 1;
    }
    for ( int i = 0 ; i < days80 ; ++i){
        long long unsigned int newFishes = states[0];
        std::rotate(states.begin(),states.begin()+1,states.end());
        states[6] += newFishes;
    }
    for(auto x : states){
            sum+= x;
        }
    std::cout << "Sum after " << days80 << " is " << sum << std::endl;

    for ( int i = days80 ; i < days256 ; ++i){
        long long unsigned int newFishes = states[0];
        std::rotate(states.begin(),states.begin()+1,states.end());
        states[6] += newFishes;
    }
    sum = 0;
    for(auto x : states){
            sum+= x;
        }
    std::cout << "Sum after " << days256 << " is " << sum << std::endl;
    
    
}