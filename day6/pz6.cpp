#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

class Fish{

    public:
    Fish(int days):m_counter(days){};
    Fish* dayPassed(){
        --m_counter;
        if( m_counter < 0 ){
            m_counter = 6;
            return new Fish(8);
        }
        return nullptr;
    };
    int getDays(){return m_counter;};
    private:
    int m_counter;
};

int main(){
    std::string fileName("in6.txt");
    std::fstream file;
    std::string number;

    std::vector<Fish*> school;

    file.open(fileName);
    while(!file.eof()){
        std::getline(file,number,',');
        school.push_back(new Fish(std::stoi(number)));
    }
    for(int i = 0 ; i < 80 ; ++i){
        std::vector<Fish*> newSchool;
        Fish* newFish = nullptr;
        for(auto f : school){
            newFish = f->dayPassed();
            if( newFish != nullptr ){
                newSchool.push_back(newFish);
            }
        }
        if(newSchool.size() > 0){
            school.insert(school.end(),newSchool.begin(),newSchool.end());
        }
    }
    std::cout << "After 80 days there is : " << school.size() << " fish" << std::endl; 
    
}