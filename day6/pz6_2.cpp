#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>

class Fish{

    public:
    Fish(int days):m_counter(days){};
    Fish* dayPassed(){
        --m_counter;
        if( m_counter < 0 ){
            m_counter = 6;
            return new Fish(8);
        }
        return nullptr;
    };
    int getDays(){return m_counter;};
    private:
    int m_counter;
};

long long unsigned int getOffspring(int daysToEnd){
    static long long unsigned int i = 0;
    while(daysToEnd > 0){
        getOffspring(daysToEnd - 9);
        daysToEnd -= 7;
    }
    i+=1;
    return i;
}

int main(){
    std::string fileName("in6.txt");
    std::fstream file;
    std::string number;

    std::vector<Fish*> school;
    int days = 256;

    file.open(fileName);
    while(!file.eof()){
        std::getline(file,number,',');
        school.push_back(new Fish(std::stoi(number)));
    }
    // for(int i = 0 ; i < days ; ++i){
    //     std::vector<Fish*> newSchool;
    //     Fish* newFish = nullptr;
    //     for(auto f : school){
    //         newFish = f->dayPassed();
    //         if( newFish != nullptr ){
    //             newSchool.push_back(newFish);
    //         }
    //     }
    //     if(newSchool.size() > 0){
    //         school.insert(school.end(),newSchool.begin(),newSchool.end());
    //     }
    // }
    // std::cout << "After " << days << " days there is : " << school.size() << " fish" << std::endl; 
    std::cout << " YOU MAY CANCEL IT, IT WILL CALCULATE FOR MORE THAN HOUR AND HALF" << std::endl;
    for( int i = 0 ; i < school.size() ;++i){
        std::cout << "Total fishes after " << i << " fish " << getOffspring( days - school[i]->getDays()) << std::endl;
    }
    
}