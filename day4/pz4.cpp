#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <list>
#include <string>
#include <sstream>
#include <algorithm>

constexpr auto CHECK = 1;
constexpr auto UNCHECK = 0;

constexpr auto BINGO = 5;

class Number{
    public:
        Number(int n){
            m_value = n;
            m_check = UNCHECK;
        }
        void setCheck(){
            m_check = CHECK;
        }
        bool isChecked(){
            return m_check == CHECK;
        }
        int getValue(){
            return m_value;
        }

    bool operator == (int const & cmp){
        return (m_value == cmp) ;
    }

    int operator + (Number const & obj){
        return m_check + obj.m_check;
    }

    int operator + (int const & obj){
        return m_check + obj;
    }

    private:
        int m_value;
        int m_check;
};

class BingoBoard{
    public:
        BingoBoard():m_board( {
                              Number(0),Number(0),Number(0),Number(0),Number(0),
                              Number(0),Number(0),Number(0),Number(0),Number(0),
                              Number(0),Number(0),Number(0),Number(0),Number(0),
                              Number(0),Number(0),Number(0),Number(0),Number(0),
                              Number(0),Number(0),Number(0),Number(0),Number(0)
                              })
                              {};

        void setupRow(std::array<Number,BINGO> row,int rowNumber){
            if(rowNumber >= BINGO){
                return;
            }
            m_board[rowNumber] = row;
        }

        bool insertNumberValue(int numberValue){
            int rowCntr = 0;
            for( auto& row : m_board ){
                int columnCntr = 0;
                for ( auto& valueOnBoard : row){
                    //std::cout << valueOnBoard.getValue() << " == " << numberValue << std::endl;
                    if ( valueOnBoard == numberValue ) {
                        valueOnBoard.setCheck();
                        //Check if bingo occured;
                        if(checkForBingo(rowCntr,columnCntr)){
                            //CALCUATE UNMARKED SUM;
                            std::cout << "BINGO VALUE " << numberValue << std::endl;
                            int unmarkedSum = getUnmarkedSum();
                            std::cout << "Final score : " << numberValue*unmarkedSum << std::endl;
                            exit(0);
                        }
                    }
                    ++columnCntr;
                }
                ++rowCntr;
            }
        }
        bool checkForBingo(int x, int y){
            int tryBingo = 0;
            for(int i = 0 ; i < BINGO ; ++i){
                tryBingo = m_board[x][i] + tryBingo;
            }

            if(tryBingo == BINGO){
                std::cout << "BINGO! " << std::endl;
                return true;
            }
            tryBingo = 0;
            for(int i = 0 ; i < BINGO ; ++i){
                tryBingo = m_board[i][y] + tryBingo;
            }
            if(tryBingo == BINGO){
                std::cout << "BINGO! " << std::endl;
                return true;
            }
            return false;
        }

        int getUnmarkedSum(){
            int sum = 0;
            for(auto x : m_board){
                for(auto y : x){
                    if(!y.isChecked()){
                        sum += y.getValue();
                    }
                }
            }
            std::cout << "UNMARKED SUM : " << sum << std::endl;
            return sum;
        }

        void print(){
            for(auto x : m_board){
                for(auto y : x){
                    std::cout << y.getValue() << " ";
                }
                std::cout << std::endl;
            }
        }

    private:
        std::array<std::array<Number,BINGO>,BINGO> m_board;
        
};

bool fillList(std::fstream& in, std::list<int>& listToFill, char delim){
    std::string tempString;
    std::getline(in,tempString);
    if(tempString.length() < 1){
        return false;
    }
    std::replace(tempString.begin(),tempString.end(),delim,'\n');
    std::stringstream stringStream(tempString);
    while(!stringStream.eof()){
        int value;
        std::string xtempString;
        std::getline(stringStream,xtempString);
        if(xtempString == ""){
            continue;
        }
        value = std::stoi(xtempString);
        listToFill.push_back(value);
    }
    return true;
}



int main(){
    std::string fileName("in4.txt");
    std::string tempString;
    
    std::list<int> drawnNumbers;
    std::list<int> helperList;

    std::vector<BingoBoard> boards = {};

    std::fstream file;
    file.open(fileName,std::fstream::in);
    
    fillList(file,drawnNumbers,',');

    std::cout << std::endl << "Drawn numbers: ";
    for(auto i : drawnNumbers){
        std::cout << i << " - ";
    }

    while(!file.eof()){
        std::vector<std::vector<int>> matrix;
        std::cout << "BOARD: " << std::endl;
        while(fillList(file,helperList,' ')){
            std::vector<int> helperVector {std::make_move_iterator(std::begin(helperList)),std::make_move_iterator(std::end(helperList))};
            matrix.push_back(helperVector);
            helperList.clear();
            helperVector.clear();
        }
        int i = 0;
        BingoBoard bb;
        for(auto x : matrix){
            for(auto y: x){
                std::cout << y << " ";
            }
            std::array<Number,BINGO> newRow = {Number(x[0]),Number(x[1]),Number(x[2]),Number(x[3]),Number(x[4])};
            bb.setupRow(newRow,i++);
            std::cout << std::endl;   
        }        
        std::cout << "BINGO BOARD " << std::endl;
        bb.print();
        std::cout << std::endl;
        if(matrix.size() == BINGO ){
            boards.push_back(bb);
        }
    }
    
    for(auto &board : boards){
            board.print();
            std::cout << std::endl;
        }

    for(auto nb : drawnNumbers){
        std::cout << "Inserting " << nb << std::endl;
        for(auto &board : boards){
            board.insertNumberValue(nb);
        }
    }
}

