from typing import MappingView
import numpy as np
from matplotlib import pyplot as plt
from numpy.lib.function_base import diff

def euclidean(x):
    return np.sqrt(x.dot(x))

def parse():
    with open("ex19_s4.txt") as file:
        results = []
        probe = []
        for line in file.readlines():
            line = line.strip()
            if not line:
                continue        
            if line.startswith('---'):
                if not probe:
                    continue
                results.append(probe.copy())
                probe = []
                continue
            coords = [int(c) for c in line.split(',')] 
            probe.append(np.array(coords))
    results.append(probe.copy())
    return results

def find2Closest(point, cloud):
    max1 = 9000
    closest1 = None
    max2 = 9000
    closest2 = None
    for p in cloud:
        if (p != point).all():
            _magnitude = euclidean(point-p)
            # print(_magnitude)
            if _magnitude < max1:
                closest1 = p
                max1 = _magnitude
            elif _magnitude < max2:
                closest2 = p
                max2 = _magnitude
            else:
                continue
    return closest1,closest2


            

rotX = np.array([[1,0,0],[0,0,-1],[0,1,0]])
rotY = np.array([[0,0,1],[0,1,0],[-1,0,0]])
rotZ = np.array([[0,-1,0],[1,0,0],[0,0,1]])

_ones = np.array([1,1,1])


def matchSimilar(setA,setB):
    mapA = createMap(setA)
    mapB = createMap(setB)
    similarPlanes = []
    similarVectors = []
    for k in mapA:
        print("%s : %s"%(k,mapA[k]))
    for k in mapB:
        print("%s : %s"%(k,mapB[k]))    

    for magnitude in mapA.keys():
        if magnitude in list(mapB.keys()):
            if (np.abs(mapB[magnitude]) == np.abs(mapA[magnitude])).any():
                similarPlanes.append((mapA[magnitude],mapB[magnitude]))
                normalA,x = getNormalToPlane(mapA[magnitude])
                normalB,normalC = getNormalToPlane(mapB[magnitude])
                similarVectors.append((normalA,x,normalB,normalC))
    return similarPlanes, similarVectors

def getNormalToPlane(plane):
    A = plane[0,:]
    B = plane[1,:]
    C = plane[2,:]
    v1 = np.cross(A-B,B-C)
    v2 = np.cross(A-C,A-B)
    return v1,v2

def computeNormals(cloud):
    normal = []
    for A in cloud:
        B,C = find2Closest(A,cloud)
        normal.append(np.cross(A-B,A-C))
    return normal

def closestPlaneAndNormal(A,cloud):
    B,C = find2Closest(A,cloud)
    normal = np.cross(A-B,A-C)
    plane = np.array([A,B,C])
    return plane, normal

def planeAndNormal(A,B,C):
    if (A == B).all() or (A == C).all() or (B == C).all():
        return None,None
    normal  = np.cross(A-B,A-C)
    plane = np.array([A,B,C])
    return plane,normal

def createMap(setA):
    list = []
    limit = 2000
    for A in setA:
        # for B in setA:
            # for C in setA:  
                # if not (np.abs(A) < limit).any() or not (np.abs(B) < limit).any() or not (np.abs(C) < limit).any():
                    # continue
        P,N = closestPlaneAndNormal(A,setA)
        if P is None:
            continue
        normalMagnitude = euclidean(N)
        list.append((normalMagnitude,P))
    _map = dict(list)
    return _map  

def createGraph(tupleList):
    graph = {}
    for i in tupleList:
        graph.setdefault(i[0],[]).append(i[1])
    reverse = [ (x[1],x[0]) for x in tupleList ]
    for i in reverse:
        graph.setdefault(i[0],[]).append(i[1])
    return graph

def Dijkstra(G,s):
    maxSize = 9000
    d = {}    
    path = {}
    for v in G.keys():
            d.update({v:maxSize})
    d[s] = 0
    path[s] = -1
    
    q = dict(d)
    while len(q) > 0:
        u = min(q, key = lambda k: q[k])
        q.pop(u)
        for v in G[u]:
            #y,x = v
            if d[v] > d[u] + (1 if 0 in G[v] else maxSize ) or d[v] == maxSize:
                q.update({v : d[u] + 1})
                d.update({v : d[u] + 1})
                path.update({v:u})
    return path

def applyTransformations(pattern, _set,isReversed = False):
    (flag, seq, t) = pattern
    if not isReversed:
        output = [ applyMatrix(seq,x) + t for x in _set ]
    else:
        reversedSeq = [np.transpose(y) for y in reversed(seq)]
        output = [ applyMatrix(reversedSeq,x) - t for x in _set ]
    return output
    
def applyMatrixOnPlane(sequence, plane):
    x = plane
    for mat in sequence:
        x = mat@x
    return x

def applyMatrix(sequence, point):
    x = point
    for mat in sequence:
        x = mat@x
    return x


def alignVectors(vRef,vTarget):
    aligned = False
    rotations = (rotX,rotY,rotZ)
    rotLetters = ('X','Y','Z')
    r = 0 
    rotationsList = []
    watchDog = 0
    while not (vRef == vTarget).all():
        watchDog += 1
        if watchDog > 24:
            # print(vRef, vTarget)
            return None
        aligned = False
        if ( vRef == vTarget ).any():
            #rotuj wedlug tej osi ktora pasuje
            r = 0
            for axis in (vRef == vTarget):
                if axis:
                    vTarget = rotations[r]@vTarget 
                    rotationsList.append(rotations[r])
                    aligned = True
                    break
                r += 1
            if aligned:
                continue
        elif (np.abs(vRef) == np.abs(vTarget)).any():
            #wyrownaj osie ktore pasuja
            r = 0
            for axis in (np.abs(vRef) == np.abs(vTarget)):
                if axis:
                    vTarget = rotations[r-1]@rotations[r-1]@vTarget
                    rotationsList.append(rotations[r-1])
                    rotationsList.append(rotations[r-1])
                    aligned = True
                    break
                r += 1
            if aligned:
                continue
        else:
            #rotuj dowolnie
            vTarget = rotX@vTarget
            rotationsList.append(rotX)
    
    print("%s -- %s"%(vRef,vTarget))
    return rotationsList 
    
        
def findTranslation(diffList):
    reference = diffList[0]
    for i in range(0,3):
        d = reference[i]
        for j in range(1,len(diffList)):
            for k in range(0,3):
                if (diffList[j][k,:] == reference[i,:]).all():
                    print(reference[i,:])
                    #chyba tyle wystarczy
                    return reference[i,:]
 


def matchingAlgorithm(cloudA, cloudB):
    planes, normals = matchSimilar(cloudA,cloudB)
    checkMatrix = []
    if len(planes) < 4:
        print("Not enough planes, found only %d!"%len(planes))
        return (False, np.eye(3), np.eye(3)) 
    for n in range (0,len(normals)):
        N = normals[n]
        sequence = alignVectors(N[0],N[2])
        if sequence is None:
            sequence = alignVectors(N[1],N[2])
        
        refA = planes[n][0][0]
        refTargetA = planes[n][1][0]
        refTargetB = planes[n][1][1]
        refTargetC = planes[n][1][2]

        refTargetA = applyMatrix(sequence,refTargetA)
        refTargetB = applyMatrix(sequence,refTargetB)
        refTargetC = applyMatrix(sequence,refTargetC)
        
        diffMatrix = np.array([refA - refTargetA,refA - refTargetB,refA - refTargetC])
        checkMatrix.append(diffMatrix)
        R = sequence
    T = findTranslation(checkMatrix)
    if (T > 2000).any():
        (False,R,T)
    return (True,R,T)

if __name__ == '__main__':
    result = parse()
    
    transformations = {}
    for i in range(0,len(result) - 1):
        for j in range(i+1, len(result)):
            scannerA = result[i]
            scannerB = result[j]
            print("Scanner %d --> Scanner %d"%(i,j))
            pattern = matchingAlgorithm(scannerA,scannerB)
            if pattern[0]:
                transformations[(i,j)] = pattern
    
    G = createGraph(transformations.keys()) 
    shortestPaths = Dijkstra(G,0) 

    answer = [x for x in result[0]]
    print(transformations.keys()) 
    for probe in range(1,len(result)):        
        currentState = probe
        newPoints = result[probe]
        while(True):
            nextState = shortestPaths[currentState]
            try:
                newPoints = applyTransformations(transformations[(currentState,nextState)],newPoints) 
            except KeyError:
                newPoints = applyTransformations(transformations[(nextState,currentState)],newPoints)
            if nextState == 0:
                break
            currentState = nextState
        answer.extend(newPoints)
    X = [ x.tolist() for x in answer]
    Y = [''.join(str(x)) for x in X ]
    
    print("Found %d unique probes" % len(list(set(Y))))