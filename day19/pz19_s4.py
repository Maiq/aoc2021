import numpy as np

def euclidean(x):
    return np.sqrt(x.dot(x))

def parse():
    with open("ex19_sum01.txt") as file:
        results = []
        probe = []
        for line in file.readlines():
            line = line.strip()
            if not line:
                continue        
            if line.startswith('---'):
                if not probe:
                    continue
                results.append(probe.copy())
                probe = []
                continue
            coords = [int(c) for c in line.split(',')] 
            probe.append(np.array(coords))
    results.append(probe.copy())
    return results

def findClosest(arr):
    neigbours = []
    for i in range(0,len(arr)):
        sPoint = arr[i]
        max1 = 2000
        max2 = 2000
        closest1 = -1
        closest2 = -2
        for j in range(0,len(arr)):
            if i == j:
                continue
            lenNorm = euclidean(sPoint-arr[j])
            if lenNorm < max1:
                closest1 = j
                max1 = lenNorm
            elif lenNorm < max2:
                closest2 = j
                max2 = lenNorm
        neigbours.append((closest1,closest2))
    return neigbours
            

def createTriangles(beacon,neigbours,scannerResult):
    A = scannerResult[beacon]
    B = scannerResult[neigbours[0]]
    C = scannerResult[neigbours[1]]

    ab = A-B
    ac = A-C
    ba = B-A
    bc = B-C
    ca = C-A
    cb = C-B

    return (ab,ac,ba,bc,ca,cb)


result = parse()
for xA in range(0,len(result)):
    for xB in range(xA+1,len(result)):
        scanner0 = result[xA]
        scanner1 = result[xB]
        sasiady0 = findClosest(scanner0)
        sasiady1 = findClosest(scanner1)

        _ones = np.array([1,1,1])


        pos0 = []
        pos1 = []

        for i in range(0,len(scanner0)):
            
            beacon0 = createTriangles(i,sasiady0[i],scanner0)
            for j in range(0,len(scanner1)):  
                beacon1 = createTriangles(j,sasiady1[j],scanner1)
                div = np.power(np.divide(beacon0[0],beacon1[0]),2)
                if (div==_ones).all():
                    # print( " Sensor0 %d Sensor1 %d"%(i,j))
                    # print(beacon0)
                    # print(beacon1)
                    try:
                        x = pos1.index(j)
                        pos0.pop(x)
                        pos1.pop(x)
                    except:
                        pos0.append(i)
                        pos1.append(j)    
                    
        print("Filtered %d -> %d:"%(xA,xB))
        # print(pos0)
        for i in range (0 ,len(pos0)):
            print( " Sensor0 %d Sensor1 %d"%(pos0[i],pos1[i]))
            print(" %s ---  %s"% (scanner0[pos0[i]],scanner1[pos1[i]]))

rotY = np.array([[0,0,1],[0,1,0],[-1,0,0]])
rotZ = np.array([0,-1,0],[1,0,0],[0,0,1]])