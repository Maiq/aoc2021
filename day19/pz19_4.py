from os import close
from typing import MappingView
import numpy as np
from matplotlib import pyplot as plt
from numpy.lib.function_base import diff

rotX = np.array([[1,0,0],[0,0,-1],[0,1,0]])
rotY = np.array([[0,0,1],[0,1,0],[-1,0,0]])
rotZ = np.array([[0,-1,0],[1,0,0],[0,0,1]])

R0 = np.eye(3,3)
R1 = rotX@R0
R2 = rotX@R1
R3 = rotX@R2

R4 = np.transpose(rotX)@np.transpose(rotZ)@R0
R5 = rotX@R4
R6 = rotX@R5
R7 = rotX@R6

R8 = rotX@rotY@R0
R9 = rotX@R8
R10 = rotX@R9
R11 = rotX@R10

R12 = rotX@rotX@rotZ@rotZ@R0
R13 = rotX@R12
R14 = rotX@R13
R15 = rotX@R14

R16 = np.transpose(rotX)@rotZ@R0
R17 = rotX@R16
R18 = rotX@R17
R19 = rotX@R18

R20 = np.transpose(rotX)@np.transpose(rotY)@R0
R21 = rotX@R20
R22 = rotX@R21
R23 = rotX@R22

R = [R0,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,R16,R17,R18,R19,R20,R21,R22,R23]


def parse():
    with open("in19.txt") as file:
        results = []
        probe = []
        for line in file.readlines():
            line = line.strip()
            if not line:
                continue        
            if line.startswith('---'):
                if not probe:
                    continue
                results.append(probe.copy())
                probe = []
                continue
            coords = [int(c) for c in line.split(',')] 
            probe.append(np.array(coords))
    results.append(probe.copy())
    return results

def euclidean(x):
    return np.sqrt(x.dot(x))

def n_closest(n,A,cloud):
    n_list = [ ((euclidean(A-x)),x) for x in cloud]
    n_list.sort()
    return n_list[1:n+1]    

def diffClouds(cloudA,cloudB):
    result = np.zeros(3)
    for A in cloudA:
        for B in cloudB:
            if (A-B).tolist() in result.tolist():
                print("To juz bylo!")
            else:
                result = np.vstack((result,A-B))
                print(len(result))
            
def manhattan(p1,p2):
    return np.abs(p1[0]-p2[0])+np.abs(p1[1]-p2[1])+np.abs(p1[2]-p2[2])
    
def distanceMatch(ref, pointAndDistance):
    for p, pList in pointAndDistance:
        for dist, point in pList:
            if ref == dist:
                #print("This reference match with distance between %s and %s"%(p,point))
                return (p,point)
    return None
            
def createGraph(tupleList):
    graph = {}
    for i in tupleList:
        graph.setdefault(i[0],[]).append(i[1])
    reverse = [ (x[1],x[0]) for x in tupleList ]
    for i in reverse:
        graph.setdefault(i[0],[]).append(i[1])
    return graph

def Dijkstra(G,s):
    maxSize = 9000
    d = {}    
    path = {}
    for v in G.keys():
            d.update({v:maxSize})
    d[s] = 0
    path[s] = -1
    
    q = dict(d)
    while len(q) > 0:
        u = min(q, key = lambda k: q[k])
        q.pop(u)
        for v in G[u]:
            #y,x = v
            if d[v] > d[u] + (1 if 0 in G[v] else maxSize ) or d[v] == maxSize:
                q.update({v : d[u] + 1})
                d.update({v : d[u] + 1})
                path.update({v:u})
    return path

def applyTransformations(T,points):
    flag, r, t = T
    return [ (r@p + t) for p in points]

def npArrayToText(arr):
    x = ""
    for i in arr.tolist():
        x = x+str(i)
        x = x+","
    return x[:-1]

if __name__ == '__main__':
    result = parse()
    
    # transformations = {}
    # for i in range(0,len(result) - 1):
    #     for j in range(i+1, len(result)):
    #         scannerA = result[i]
    #         scannerB = result[j]
    #         print("Scanner %d --> Scanner %d"%(i,j))
    #         pattern = matchingAlgorithm(scannerA,scannerB)
    #         if pattern[0]:
    #             transformations[(i,j)] = pattern
    

    transformations = {}
    for i in range(0,len(result)-1):
        for j in range(0, len(result)):
            if i == j:
                continue
            cA = result[i]
            cB = result[j]

            closestA = [(x,n_closest(2,x,cA)) for x in cA]
            closestB = [(x,n_closest(2,x,cB)) for x in cB]
            trueR = None
            trueT = None
            if len(closestA) < 24:
                continue
            if len(closestB) < 24:
                continue
            for source, neighbours in closestA:
                for distance, point in neighbours:
                    match = distanceMatch(distance,closestB)
                    if match is not None:
                        print("Overlap between %d and %d"%(i,j))
                        # print("Source %s --- %s map to match -> %s --- %s "%(source,point,match[0],match[1]))
                        print("Vector %s maps to %s"%(source-point, match[0]-match[1]))
                        vRef = source-point
                        vTarget = match[0]-match[1]
                        for r in R:
                            if (vRef == r@vTarget).all():
                                #print("Matches with rotation \n%s"%r)
                                translations = source - r@match[0]
                                trueT = translations
                                trueR = r
                                print("Overlap is %s -> %s"%(source,match[0]))
                                print("Overlap is %s -> %s"%(point,match[1]))
                                print("Translation is %s"%translations)
                                continue
                            if ( vRef == r@(-vTarget)).all():
                                #print("Matches with swapped coordinates \n%s"%r)
                                translations = source - r@match[1]
                                trueT = translations
                                trueR = r
                                print("Overlap is %s -> %s"%(source,match[1]))
                                print("Overlap is %s -> %s"%(point,match[0]))
                                print("Translation is %s"%translations)
                                continue
                if trueR is not None:
                    transformations[(i,j)] = (False,trueR,trueT)

    
    G = createGraph(transformations.keys()) 
    shortestPaths = Dijkstra(G,0) 

    # answer = [x for x in result[0]]
    test = (True,R0,np.array([0,0,0]))
    answer = [x for x in applyTransformations(test,result[0]) ]
    answer2 = []
    print(transformations.keys()) 
    for probe in range(1,len(result)):     
        currentState = probe
        newPoints = result[probe]
        nextState = shortestPaths[currentState]
        scannerPosition = transformations[(nextState,currentState)][2]
        while(True):
            nextState = shortestPaths[currentState]
            
            print(nextState)
            # try:
            newPoints = applyTransformations(transformations[(nextState,currentState)],newPoints) 
            # except KeyError:
            #     newPoints = applyTransformations(transformations[(currentState,nextState)],newPoints)
            if nextState == 0:
                print(scannerPosition)
                answer2.append(scannerPosition)
                break
            else:
                scannerPosition = transformations[(shortestPaths[nextState],nextState)][1]@scannerPosition + transformations[(shortestPaths[nextState],nextState)][2]
            currentState = nextState
        answer.extend(newPoints)
    X = [ x.tolist() for x in answer]
    Y = [''.join(str(x)) for x in X ]
    
    print("Found %d unique probes" % len(list(set(Y))))

    # print("Probe 0")
    # for x in result[0]:
    #     print(npArrayToText(x))
    # for x in applyTransformations(transformations[(0,1)],result[1]):
    #      print(npArrayToText(x))

    maxManhattan = 0
    for i in range(0,len(answer2)):
        for j in range(0,len(answer2)):
            if i == j:
                continue
            distManhattan = manhattan(answer2[i],answer2[j])
            if distManhattan > maxManhattan:
                maxManhattan = distManhattan
    print("Max manhattan distance is %d"%maxManhattan)