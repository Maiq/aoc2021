import numpy as np
from matplotlib import pyplot as plt

def euclidean(x):
    return np.sqrt(x.dot(x))

def parse():
    with open("ex19_s4.txt") as file:
        results = []
        probe = []
        for line in file.readlines():
            line = line.strip()
            if not line:
                continue        
            if line.startswith('---'):
                if not probe:
                    continue
                results.append(probe.copy())
                probe = []
                continue
            coords = [int(c) for c in line.split(',')] 
            probe.append(np.array(coords))
    results.append(probe.copy())
    return results

def find2Closest(point, cloud):
    max1 = 9000
    closest1 = None
    max2 = 9000
    closest2 = None
    for p in cloud:
        if (p != point).all():
            _magnitude = euclidean(point-p)
            # print(_magnitude)
            if _magnitude < max1:
                closest1 = p
                max1 = _magnitude
            elif _magnitude < max2:
                closest2 = p
                max2 = _magnitude
            else:
                continue
    # if closest1 is None or closest2 is None:
    #     print(point)
    #     print("LOL!")
    #     exit()
    return closest1,closest2
 
            
def n_closest(n, point, cloud):
    _max = [9000]*n
    for other in cloud:
        if (point == other).all():
            continue
        distance = euclidean(point-other)
        if distance < _max[-1]:
            _max[-1] = distance
            _max.sort()
    return _max

def meanDistance(distance):
    dist = np.array(distance)
    return np.mean(dist)


rotX = np.array([[1,0,0],[0,0,-1],[0,1,0]])
rotY = np.array([[0,0,1],[0,1,0],[-1,0,0]])
rotZ = np.array([[0,-1,0],[1,0,0],[0,0,1]])

_ones = np.array([1,1,1])


def leastSquare(value, _set):
    _max = 9000.00
    index = 0
    cntr = 0
    for x in _set:
        diffSquared = np.power((value-x),2)
        if diffSquared < _max:
            _max = diffSquared
            index = cntr
        cntr+=1
    return index, _max  

def checkColumns(output):
    for i in range(0,3):
            if np.all(output[:,i]==output[0,i]):
                return i
    return -1

def doesAllColumnsMatch(output):
    if np.all(output[:,0]==output[0,0]) == True:
        if np.all(output[:,1]==output[0,1]) == True:
            if np.all(output[:,2]==output[0,2]) == True:
                return True    
    return False

def apply_n_matrix(n,rot,input):
    out = input
    for i in range(0,n):
        out = rot@out
    return out


def matchSimilar(setA,setB):
    pairs = []
    meansA = [meanDistance(n_closest(2,A,setA)) for A in setA]
    meansB = [meanDistance(n_closest(2,B,setB)) for B in setB]
    idx = 0
    for A in meansA:
        matchingPointIdx, least = leastSquare(A,meansB)
        # print ("Least diff %d for %d = %f"%(idx,matchingPointIdx,least))
        if least == 0:
            pairs.append((setA[idx],setB[matchingPointIdx]))
        idx += 1
    return pairs


def createGraph(tupleList):
    graph = {}
    for i in tupleList:
        graph.setdefault(i[0],[]).append(i[1])
    reverse = [ (x[1],x[0]) for x in tupleList ]
    for i in reverse:
        graph.setdefault(i[0],[]).append(i[1])
    return graph

def Dijkstra(G,s):
    maxSize = 9000
    d = {}    
    path = {}
    for v in G.keys():
            d.update({v:maxSize})
    d[s] = 0
    path[s] = -1
    
    q = dict(d)
    while len(q) > 0:
        u = min(q, key = lambda k: q[k])
        q.pop(u)
        for v in G[u]:
            #y,x = v
            if d[v] > d[u] + (1 if 0 in G[v] else maxSize ) or d[v] == maxSize:
                q.update({v : d[u] + 1})
                d.update({v : d[u] + 1})
                path.update({v:u})
    return path

def applyTransformations(pattern, _set,isReversed = False):
    (flag, seq, t) = pattern
    if not isReversed:
        output = [ applyMatrix(seq,x) + t for x in _set ]
    else:
        reversedSeq = [np.transpose(y) for y in reversed(seq)]
        output = [ applyMatrix(reversedSeq,x) - t for x in _set ]
    return output
    
def applyMatrix(sequence, point):
    x = point
    for mat in sequence:
        x = mat@x
    return x

def tryEverything(matchingPair):
    rotations = (rotX,rotY,rotZ)
    sequence = [ [rotX,rotY,rotX], [rotZ,rotY,rotZ], [rotY,rotZ,rotY],[rotX,rotZ,rotX],[rotY,rotX,rotY],[rotZ,rotX,rotZ]]
    column = -1
    k = -1
    for i in range(0,6):
        # output = np.array([p[0] - applyMatrix(sequence[i],p[1]) )
        output = np.array([p[0] - applyMatrix(sequence[i],p[1]) for p in matchingPair])
        column = checkColumns(output)
        if column != -1:
            print("FOUND SOMETHING!")
            k = i
            break
    seq = sequence[k]
    for i in range(0,4):
        seq.append(rotations[column])
        output = np.array([p[0] - applyMatrix(seq,p[1]) for p in matchingPair])
        if doesAllColumnsMatch(output):
            # print( " Matching columns after %d rotations"%i)
            print(output)
            return True, seq ,output[0,:]

def matchingAlgorithm(scannerA, scannerB):
        matchingPair = matchSimilar(scannerA,scannerB)
        if len(matchingPair) < 12:
            # print("Couldn't find enough matching pairs for this set")
            return (False, 0, np.eye(3), np.eye(3), np.zeros(3) )
        rotations = (rotX,rotY,rotZ)
        rotLetters = ('X','Y','Z')
        firstRot = rotZ
        secondRot = rotX
        firstAxis = ""
        secondAxis = ""
        for r in range(0,3):
            R=rotations[r]
            output = np.array([p[0] - R@p[1] for p in matchingPair])
            # print(output)
            # print("____")
            column = checkColumns(output)
            if column != -1:
                # print("Rotation in %s gave matching column %s" % (rotLetters[r],rotLetters[column]))
                firstRot = rotations[r]
                secondRot = rotations[column]
                firstAxis = rotLetters[r]
                secondAxis = rotLetters[column]
                break
        for i in range(0,4):
            output = np.array([ p[0] - apply_n_matrix(i,secondRot,firstRot@p[1]) for p in matchingPair])
            if doesAllColumnsMatch(output):
                # print( " Matching columns after %d rotations"%i)
                print(output)
                print( " Transformations %d %s , %s  move by %s"%(i,secondAxis,firstAxis, output[0,:]) )
                sequence = [firstRot] + [secondRot]*i
                return True, sequence ,output[0,:]
        print("Hej")
        return tryEverything(matchingPair)
                
def computeNormal(cloud):
    normal = []
    for A in cloud:
        B,C = find2Closest(A,cloud)
        normal.append(np.cross(A-B,A-C))
    return normal


if __name__ == '__main__':
    result = parse()
    
    transformations = {}
    for i in range(0,len(result) - 1):
        for j in range(i+1, len(result)):
            scannerA = result[i]
            scannerB = result[j]
            print("Scanner %d --> Scanner %d"%(i,j))
            pattern = matchingAlgorithm(scannerA,scannerB)
            if pattern[0]:
                transformations[(i,j)] = pattern
    
    G = createGraph(transformations.keys()) 
    shortestPaths = Dijkstra(G,0) 

    answer = [x for x in result[0]]
    
    for probe in range(1,len(result)):        
        currentState = probe
        newPoints = result[probe]
        while(True):
            nextState = shortestPaths[currentState]
            try:
                newPoints = applyTransformations(transformations[(nextState,currentState)],newPoints) 
            except KeyError:
                newPoints = applyTransformations(transformations[(currentState,nextState)],newPoints)
            if nextState == 0:
                break
            currentState = nextState
        answer.extend(newPoints)
    X = [ x.tolist() for x in answer]
    Y = [''.join(str(x)) for x in X ]
    
    print("Found %d unique probes" % len(list(set(Y))))

    # for key in transformations:
    #     print(key, ":" , transformations[key][-1])
    
    # newPoints = [transformations[(2,3)][-1]]
    # print(newPoints)
    # newPoints = applyTransformations(transformations[(0,2)],newPoints)
    # print(newPoints)
    
    # for l in list(set(Y)):
    #     test = l.split(' ')
    #     p = ''+test[0]+test[1]+test[2]
    #     print(p[1:-1])

    normals = computeNormal(result[0])
    for n in normals:
        print(euclidean(n))
    print("_______")
    normals1 = computeNormal(result[1])
    for n in normals1:
        print(euclidean(n))
    
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    cloud = result[0]
    for A in cloud:
        B,C = find2Closest(A,cloud)
        print(A)
        print(B)
        print(C)
        plane = np.array([A,B,C])
        print(plane)
        ax.scatter(plane[:,0],plane[:,1],plane[:,2])
        cog = [np.sum(plane[:,0])/3, np.sum(plane[:,1])/3, np.sum(plane[:,2])/3]
        print(cog)
        N = np.cross(A-B,A-C)
        unitN = N/np.sqrt(N.dot(N))
        end = cog + (unitN*100)
        ax.scatter(cog[0],cog[1],cog[2],marker='*')
        ax.scatter(end[0],end[1],end[2],marker='^')
        conn = np.array([cog,end])
        ax.plot(conn[:,0],conn[:,1],conn[:,2],color='black')

    plt.show()