from operator import add
import timeit 
maxSize = 100000000000000000
def parse():
    graph = []
    with open("in15.txt") as file:
        for line in file.readlines():
            row = []
            for char in line.strip():
                row.append(int(char))
            graph.append(row)

    return graph

def expandGraph(G):
    newGraph = []
    for i in range(0,5):
        for y in range(0,len(G)):
            newGraph.append( [ (x%10)+(1*int(x/10)) for x in list(map(add,G[y],[i]*len(G))) ])

    # for x in newGraph:
    #     print(x)
    

    rowLen = len(newGraph[0])
    for i in range(1,5):
        for row in newGraph:
            row.extend( [ (x%10)+(1*int(x/10)) for x in list(map(add,row[0:rowLen],[i]*rowLen))])
    
    # for x in newGraph:
    #     print(x)
    return newGraph

def adjacent(point,G):
    y,x = point
    n = [ (y,x+1), (y-1,x) , (y+1,x), (y,x-1) ]
    cntr = 0
    for elem in n:
        if elem[0] < 0:
            n.pop(cntr)
        if elem[0] >= len(G):
            n.pop(cntr)
        cntr += 1
    
    cntr = 0
    for elem in n:
        if elem[1] < 0:
            n.pop(cntr)
        if elem[1] >= len(G[0]):
            n.pop(cntr)
        cntr += 1
    return n

def Dijkstra(G,s):
    d = {}    
    path = {}
    for y in range(0,len(G)):
        for x in range(0,len(G[0])):
            d.update({(y,x):maxSize})
    d[s] = 0
    path[s] = (-1,-1)
    
    q = dict(d)
    
    while len(q) > 0:
        u = min(q, key = lambda k: q[k])
        q.pop(u)
        
        for v in adjacent(u,G):
            y,x = v
            if d[v] > d[u] + G[y][x] or d[v] == maxSize:
                q.update({v : d[u] + G[y][x]})
                d.update({v : d[u] + G[y][x]})
                path.update({v:u})
                
    return path
    

G = parse()

newGraph = expandGraph(G)# PART 2
G = newGraph# PART2

# for x in G:
#     s=""
#     for c in x:
#         s+=str(c)
#     print (s)


startPoint = (0,0)
endPoint = ((len(G)-1,len(G[0])-1))
# startPoint = ((len(G)-1,len(G[0])-1))
# endPoint = (0,0)

path = Dijkstra(G,startPoint)
maze = [['.']*len(G[0])]*len(G)
step = endPoint
bestPath = []
cost = 0
print("DIJKSTRA DONE")
while True:
    if step == startPoint:
        break
    y,x = step
   # print ("Moving from %s to %s => %d" % (step,path[step],G[y][x]))
    cost += G[y][x] 
    bestPath.append(step)
    step = path[step]
#print ("Cost is %d"%(cost))

#print(bestPath)
sum = 0
for elem in bestPath:
    y,x = elem
    sum += G[y][x]
print("Sum %d"%(sum)) 

myMap = []
cntr = 0
for x in G:
    s = '.'*len(x)
    if cntr == 0:
        s = 'x'+s[1:]
    cntr+=1
    myMap.append(s)
for elem in bestPath:
    y,x = elem
    newElem = myMap[y][:x]+str(G[y][x])+myMap[y][x+1:]
    oldString = myMap[y]
    myMap[y] = newElem
    

# for es in myMap:
#     print(es)


print("Sum %d"%(sum)) 


