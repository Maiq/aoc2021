
import sys
minInt = -sys.maxsize-1
totalFlashes = 0
bound = [minInt]*12
grid = [bound]
flashEvent = False
simultanous = 0
totalStep = 0

def parse():
    with open("in11.txt",'r') as f:
        for line in f:
            row = []
            for char in line.strip():
                row.append(int(char))
                l = [minInt] + row + [minInt]
            grid.append(l)
        grid.append(bound)

def lightUp(y,x):
    grid[y][x] +=1 

def lightFromCenter(y,x):
    for i in range(y-1,y+2):
        for j in range(x-1,x+2):
                grid[i][j] += 1

def flash(y,x):
    global totalFlashes
    global flashEvent
    if grid[y][x] > 9:
        grid[y][x] = minInt
        flashEvent = True
        totalFlashes +=1
        lightFromCenter(y,x)

def zeroDumbo(y,x):
    global simultanous
    if grid[y][x] < 0:
        grid[y][x] = 0
        simultanous +=1
        

def lightStep():
    global flashEvent
    global simultanous
    flashEvent = False
    
    for y in range(1,11):
        for x in range(1,11):
            lightUp(y,x)
            flash(y,x)

    while(flashEvent):
        flashEvent = False
        for y in range(1,11):
            for x in range(1,11):
                flash(y,x)
    simultanous = 0
    for y in range(1,11):
        for x in range(1,11):
            zeroDumbo(y,x)  

if __name__ == '__main__':
    parse()
    i = 0
    while(True):
        lightStep()
        if simultanous == 100:
            print("Simultaneous flash after %d"% (i+1))
            break
        i+=1
    
    print("Total flashed %d" %totalFlashes)
