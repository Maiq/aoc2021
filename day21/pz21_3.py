from collections import defaultdict
from collections import namedtuple


distribution = [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)]


Universe = namedtuple("Universe","count,P1,P2")

M = defaultdict (lambda: 1 ) 
#for ease I set postion 0-9 instead 1-10
originPlayer1 = 7
originPlayer2 = 4 
M[(0,0,originPlayer1, originPlayer2)] = 1

def roll(realities,player):
    newMultiverse = defaultdict (lambda: 0)
    print("There are %d"%(len(realities.keys())))
    # for k in realities:
    #     print(realities[k])
    for score in realities:
        for diceRoll, occurance in distribution:
            newPosition = (score[player+2]+diceRoll)%10
            updatedScore = score[player] + newPosition + 1
            if player == 0:
                newScore = (updatedScore,score[1],newPosition,score[3])
                newMultiverse[newScore] += realities[score]*occurance
            else:
                newScore = (score[0],updatedScore,score[2],newPosition)
                newMultiverse[newScore] += realities[score]*occurance
    return newMultiverse

def removeFinished(realities):
    winningScore = 21
    player1Wins = 0
    player2Wins = 0
    stillPlaying = realities.copy()
    for score in realities:
        if score[0] >= winningScore:
            player1Wins += realities[score]
            stillPlaying.pop(score)
        if score[1] >= winningScore:
            player2Wins += realities[score]
            stillPlaying.pop(score)
    return stillPlaying, player1Wins, player2Wins


if __name__== "__main__":
    turn = 0 
    player1 = 0
    player2 = 0
    while True:        
        print("Turn %d"%turn)
        M = roll(M,(turn%2))
        turn += 1
        # for x in M:
        #     print("%s : %s"%(x,M[x]))
        inPlay, wins1, wins2 = removeFinished(M)
        player1 += wins1
        player2 += wins2 
        print("Still playing %s : %s"%(player1,player2))
        if len(inPlay.keys()) == 0 :
            break
        else:
            M = inPlay.copy()
    print("Game finished %s : %s"%(player1,player2))
    winner = player1 if player1 > player2 else player2
    print("Winner with %d"%winner)