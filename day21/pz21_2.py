from collections import defaultdict
from collections import namedtuple

scores=[[0]*21,[0]*21]
positions=[[0]*10,[0]*10]
#origin
positions[0][3] = 1
positions[1][7] = 1
    
globalUniverse = {0:0}
currentUniverse = {}
newUniverse = {}

#distribution between points and universes in each turn 
distribution = [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)]
#originPosition shall be 0-9 instead 1-10

Universe = namedtuple("Universe","universeCount,positionP1,positionP2")

def firstTurn(origin1,origin2):
    realities = defaultdict (lambda: Universe(0,-1,-1) )
    currentReality = defaultdict (lambda: Universe(0,-1,-1) )
    newReality = defaultdict (lambda: Universe(0,-1,-1) )
    realities[(0,0)] = Universe(1,origin1,origin2)

    for score in realities:
        player2Position = realities[score].positionP2
        for points,occurance in distribution:
            newPosition = (origin1+points)%10
            playerScore = 1+newPosition
            newKey = (playerScore, score[1])
            newReality[newKey] = Universe(occurance,newPosition,player2Position)
    
    realities = newReality.copy()
    newReality = defaultdict (lambda: Universe(0,-1,-1) )
    for score in realities:
        player1Position = realities[score].positionP1
        universeFactor = realities[score].universeCount
        for points,occurance in distribution:
            newPosition = (origin2+points)%10
            playerScore = 1+newPosition
            newKey = (score[0],playerScore)
            newReality[newKey] = Universe(universeFactor*occurance,player1Position,newPosition)
    return newReality

def nextTurn(realities,p1,p2):
    newReality = defaultdict (lambda: Universe(0,-1,-1) )
    currentReality = defaultdict (lambda: Universe(0,-1,-1) )
    existentUniverses = 0
    for score in realities:
        player2Position = realities[score].positionP2
        universeFactor = realities[score].universeCount
        for points,occurance in distribution:
            newPosition = (realities[score].positionP1+points)%10
            playerScore = score[0] + newPosition + 1 
            newKey = (playerScore, score[1])
            newReality[newKey] = Universe(universeFactor*(occurance+existentUniverses), newPosition,player2Position)
        currentReality = mergeRealities(currentReality,newReality)
        newReality.clear()
    currentReality,p1,p2 = checkRealities(currentReality,p1,p2)
    
    realities = currentReality.copy()
    newReality = defaultdict (lambda: Universe(0,-1,-1))
    currentReality = defaultdict (lambda: Universe(0,-1,-1))
    
    for score in realities:
        player1Position = realities[score].positionP1
        universeFactor = realities[score].universeCount
        for points,occurance in distribution:
           newPosition = (realities[score].positionP2+points)%10
           playerScore = score[1] + newPosition + 1
           newKey = (score[0],playerScore)
           newReality[newKey] = Universe(universeFactor*(occurance+existentUniverses), player1Position,newPosition)
        currentReality = mergeRealities(currentReality,newReality)
        newReality.clear()

    currentReality,p1,p2 = checkRealities(currentReality,p1,p2)
    return currentReality,p1,p2
    
def checkRealities(realities,p1Won,p2Won):
    p1 = p1Won
    p2 = p2Won
    playingRealities = realities.copy()
    for score in realities:
        if score[0] >= 21:
            p1 += realities[score].universeCount 
            print("Player 1 won in %d universes with score %s"%(realities[score].universeCount,score))
            playingRealities.pop(score)
        elif score[1] >= 21:
            p2 += realities[score].universeCount 
            playingRealities.pop(score)
            print("Player 2 won in %d universes with score %s"%(realities[score].universeCount,score))

    return playingRealities,p1,p2
        
def mergeRealities(base,new):
    mergedReality = base 
    for score in new:
        occurance,p1,p2 = new[score]
        baseOcc,bp1,bp2 = base[score]
        mergedReality[score] = Universe(occurance+baseOcc,p1,p2)
    return mergedReality

def firstRoll(originPosition,universeFactor = 1):
    firstTurnUniverse = defaultdict(lambda : (0,-1))
    for points,occurance in distribution: 
        newPosition = (originPosition+points)%10
        firstTurnUniverse[1+newPosition] = (universeFactor*occurance,newPosition) #(numberOfUniversesWithThatScore,positionOnBoardInThoseUniverses)
    return firstTurnUniverse

def mergeUniverses(base,new):
    mergedUniverse = base
    for score in new:
        occurance,position = new[score]
        baseOcc,basePos = base[score]
        mergedUniverse[score] = ((baseOcc)+occurance, position)
    return mergedUniverse
        
def nextRoll(originUniverse,universeCount=27):
    currentUniverse = defaultdict(lambda :(0,-1))
    for score in originUniverse:
        newUniverse.clear()
        #print("Score %d with occurance %d and position %d"%(score,originUniverse[score][0],originUniverse[score][1]))
        for points,occurance in distribution:
            newScore = score + points
            originPosition = originUniverse[score][1]
            newPosition = (originPosition + points)%10
            universeFactor = universeCount*originUniverse[score][0]
            newUniverse[newScore] = (occurance*universeFactor,newPosition)
        #print(newUniverse)
        currentUniverse = mergeUniverses(currentUniverse,newUniverse)
    print(currentUniverse)
    return currentUniverse

def countAndFilterUniverse(universe):
    filteredUniverse = universe.copy()
    countUniverses = 0
    winningUniverse = 0
    for k in universe:
        countUniverses += universe[k][0]
        if  k >= 21:
            winningUniverse += universe[k][0]
            filteredUniverse.pop(k)
    if winningUniverse > 0:
        print("Won in %d"%winningUniverse)
    return filteredUniverse, countUniverses

def countRealities(reality):
    countUniverses = 0
    for k in reality:
        countUniverses += reality[k].universeCount
    return countUniverses


win1 = 0
win2 = 0
x = firstTurn(3,7)

print(countRealities(x))
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
turns = 2 

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))


x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

x,win1,win2 = nextTurn(x,win1,win2)
for score in x:
    print ("%s : %d"%(score,x[score].universeCount))
print(countRealities(x))

print(win1)
print(win2)