
pos = [7,4]
score = [0,0]

def moveInTurn(n):
    return (9*n+6)%10
n = 0
while(True):
    player = n%2
    pos[player] = (pos[player] + moveInTurn(n))%10
    score[player] += (pos[player]+1)
    print("Player %d total score%d"%(player+1,score[player]))
    if(score[player] >= 1000):
        print("Player %d wins with total score%d in round %d"%(player+1,score[player],n))
        print("Player %d loses with total score%d in round %d"%((player+1)%2,score[(player+1)%2],n))
        print("Die rolled %d"%(3*(n+1)))
        x = (3*(n+1)) * score[(player+1)%2] 
        print("Puzzle answer %d"%(x))
        break
    n += 1
    