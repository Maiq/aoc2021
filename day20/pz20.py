def sizeAndIEA(fileName):
    with open(fileName) as file:
        IEA = file.readline().strip() #IEA
        file.readline().strip() #newline
        imageSize = len(file.readline().strip()) #size
    return imageSize,IEA
def frameToInt(a,b,c):
    output = a+b+c
    output = output.replace('#','1')
    output = output.replace('.','0')
    output = int(output,2)
    return output

def getLitNumber(fileName):
    with open(fileName) as file:
        IEA = file.readline().strip() #IEA
        file.readline().strip() #newline
        totalCount = 0
        for line in file.readlines():
            totalCount += line.strip().count('#')
    return totalCount

def parse(fileName,loopCount,infChar = '.'):
    s,IEA = sizeAndIEA(fileName)
    if infChar == '.':
        infinite = '...'+'.'*(s)+'...'
        infMargs = '...'
    else:
        infinite = '###'+'#'*(s)+'###'
        infMargs = '###'
    scope = [infinite,infinite,infinite]
    f = open(fileName)
    f.readline()#drop IEA
    f.readline()#drop limiter
    
    outName = "loop"+str(loopCount)+".txt"
    out = open(outName,'w')
    out.write(IEA)
    out.write("\n")
    out.write("\n")

    
    for i in range(0,len(scope[0])-2):
        a = scope[0][i:i+3]
        b = scope[1][i:i+3]
        c = scope[2][i:i+3]
        code = frameToInt(a,b,c)
        out.write(IEA[code])
    out.write('\n')
    print("Inifite is with code %d = %s"%(code,IEA[code]))
    nextInf = IEA[code]
    for line in f.readlines():
        scope[0] = scope[1]
        scope[1] = scope[2]
        scope[2] = infMargs + line.strip()+infMargs
        for i in range(0,len(scope[0])-2):
            a = scope[0][i:i+3]
            b = scope[1][i:i+3]
            c = scope[2][i:i+3]
            code = frameToInt(a,b,c)
            out.write(IEA[code])
        out.write('\n')

    for i in range(0,3): 
        scope[0] = scope[1]
        scope[1] = scope[2]
        scope[2] = infinite
        for i in range(0,len(scope[0])-2):
            a = scope[0][i:i+3]
            b = scope[1][i:i+3]
            c = scope[2][i:i+3]
            code = frameToInt(a,b,c)
            out.write(IEA[code])
        out.write('\n')
    out.close()

    return outName, loopCount+1, nextInf 
   

fileToOpen = "in20.txt"
infChar = '.'
for i in range(0,50):
    fileToOpen, loop, infChar = parse(fileToOpen,i,infChar)
print(getLitNumber(fileToOpen))
