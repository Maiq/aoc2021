#include <iostream>
#include <regex>
#include <utility>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <list>
#include <chrono>

constexpr auto DAYS = 40;
std::map<std::string,char> rules;
std::map<char,long long unsigned int> wordCount;
std::map<std::string,long long unsigned int> nextStep;
std::map<std::string,long long unsigned int>  step;

void setUpStep(std::string polymerTemplate){
    for(auto i = 0; i < polymerTemplate.size() - 1; ++i){
        step[polymerTemplate.substr(i,2)] += 1;
    }    
}

void setUpWordCount(std::string polymerTemplate){
    for(auto i = 0 ; i < polymerTemplate.size() ; ++i){
        wordCount[polymerTemplate[i]] += 1;
    }
}

void expand(std::string xx, long long unsigned int multiplier){
    char y = rules[xx];
    std::string xy = {xx[0],y};
    std::string yx = {y,xx[1]};
    nextStep[xy] += multiplier;
    nextStep[yx] += multiplier;
    wordCount[y] += multiplier;
}

void parse(std::string& stringToParse){
    std::regex re(R"((\w\w)\s*->\s*(\w))");
    std::smatch matches;
    std::regex_search (stringToParse, matches, re);
    if(matches.size() == 3){
        rules[matches[1]] = matches[2].str()[0];
    }
}

int main(){
    auto origin = std::chrono::steady_clock::now();
    std::string fileName("in14.txt");
    std::string polymerTemplate("");
    std::string stringToParse;
    std::fstream file;
    std::list<long long unsigned int > occurances;
    file.open(fileName);
    file >> polymerTemplate;
    while(!file.eof()){
        std::getline(file,stringToParse);
        parse(stringToParse);
    }
    file.close();
    
    setUpStep(polymerTemplate);
    setUpWordCount(polymerTemplate);
    for(int i = 0 ; i < DAYS ; i++){
        nextStep.clear();
        for(auto elem : step){
            expand(elem.first,elem.second);
        }
        step.clear();
        step = nextStep;
    }
    for (auto x : wordCount){
        occurances.push_back(x.second);
    }
    auto answer = (*std::max_element(occurances.begin(),occurances.end())) - (*std::min_element(occurances.begin(),occurances.end())) ; 
    std::cout << "Answer is " << answer << std::endl;

    std::cout << "TotalTime ;"<< std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - origin).count() << "; ms " << std::endl;
    return 0;
}