#include <iostream>
#include <fstream>
#include <istream>
#include <vector>
#include <map>
#include <algorithm>

constexpr auto DOV = 4; //Digit Output Value
constexpr auto USP = 10; //Unique Signal Pattern

/*
xString Arthmetics and detection is on pattern
             aaaa 
            f    b 
            f    b 
             gggg 
            e    c 
            e    c 
             dddd 
 */

class xString: public std::string{
    public:
    xString(const char* s): std::string(s){};
    xString(const std::string& s):std::string(s){};
    
    xString operator+(const xString & arg){
        xString result(c_str());
        for(auto c : arg){
            if( find(c) == std::string::npos){
                result.push_back(c);
            }
        }
        return result;
    };

    xString operator-(const xString & arg){
        xString result(c_str());
        for(auto c : arg){
            auto pos = result.find(c);
            if(pos != std::string::npos){
                result.erase(pos,1);
            }else{
                result.push_back(c);
            }
        }
        return result;
    }
    bool operator == (const xString & arg){
        xString result(c_str());
        xString sortArg(arg.c_str());
        std::sort(result.begin(),result.end());
        std::sort(sortArg.begin(),sortArg.end());
        return static_cast<bool>(!result.compare(sortArg));
    }


};

xString xDigits[] =
{
    xString("abcdef"),  //0
    xString("bc"),      //1
    xString("abdeg"),   //2
    xString("abcdg"),   //3
    xString("bcfg"),    //4
    xString("acdfg"),   //5
    xString("acdefg"),   //6
    xString("abc"),     //7
    xString("abcdefg"), //8
    xString("abcdfg")   //9
};

// x1 means its xString coded for number 1
// xABC means its xString coded for segments ABC
// y is something that's unknown
// if there's reference I want to set this value

char detectA(xString x1, xString x7){    
    return (x1-x7)[0];// GET FIRST CHAR FROM STRING
}

char detectB(xString x1, xString y1, xString y2, xString y3, xString& x6){
    // y1, y2 ,y3 must be from set {0,6,9} 
    xString x8("abcdefg");
    if( (x1+y1) == x8){
        x6 = y1;
        return (x8-y1)[0];
    }
    else if ( (x1+y2) == x8 ){
        x6 = y2;
        return (x8-y2)[0];
    }
    else{
        x6 = y3;
        return (x8-y3)[0];
    }
}
char detectC(xString x7, xString xAB){
    return (x7-xAB)[0];
}

xString detectG_E(xString x4, xString y1, xString y2, xString& x9){
    // y1, y2 must be from set {0,9}
    xString x8("abcdefg");
    xString result("");
    if( x4+y1 == x8 ){ //Y1 is 0 Y2 is 9
         result.push_back((x8 - y1)[0]);    //G
         result.push_back((x8 - y2)[0]);    //E
         x9 = y2;
    }
    else{
        result.push_back((x8 - y2)[0]); //G
        result.push_back((x8 - y1)[0]);   //E
        x9 = y1;
    }
    return result;
}

char detectF( xString xBCG , xString x4){
    return (x4-xBCG)[0];
}

char detectD( xString x4, xString x9, xString xA){
    return (((x4+xA))-x9)[0];
}


xString getDecodedValue(xString x, std::map<char,char>& decoder ){
    xString result("");
    for(auto c : x){
        result.push_back(decoder[c]);
    }
    return result;
}

int main(){

    long totalInput = 0;
  
    std::string fileName("in8.txt");
    std::fstream file;
    std::vector<std::string> helperUSP;
    std::vector<std::string> helperDOV;
    std::map<int, std::vector<xString> > mapSizeToUSP;

    std::map<char, char> encoder;
    std::map<char, char> decoder;
    int unique = 0;

    auto comparator = [](xString x1, xString x2) -> bool
    {
        std::sort(x1.begin(),x1.end());
        std::sort(x2.begin(),x2.end());
        return x1 < x2;
    };
    std::map<xString,int,decltype(comparator) > xDigitToInt(comparator);
    xDigitToInt[xDigits[0]] = 0;
    xDigitToInt[xDigits[1]] = 1;
    xDigitToInt[xDigits[2]] = 2;
    xDigitToInt[xDigits[3]] = 3;
    xDigitToInt[xDigits[4]] = 4;
    xDigitToInt[xDigits[5]] = 5;
    xDigitToInt[xDigits[6]] = 6;
    xDigitToInt[xDigits[7]] = 7;
    xDigitToInt[xDigits[8]] = 8;
    xDigitToInt[xDigits[9]] = 9;

    
    file.open(fileName);
    while(!file.eof()){

        std::string helper;
        helperUSP.clear();
        helperDOV.clear();
        mapSizeToUSP.clear();
        encoder.clear();
        decoder.clear();
        unique = 0;

        for(int i = 0 ; i < USP ; i++){
            file >> helper;
            mapSizeToUSP[helper.size()].push_back(xString(helper));
        }
        file >> helper; //DROP DELIM

        for(int i = 0 ; i < DOV ;++i){
            file >> helper;
            if(helper.size() == 2 || helper.size() == 3 || helper.size() == 4 || helper.size() == 7 ){
                unique++;
            }
            helperDOV.push_back(helper);
        }

        //START ARTHMETIC
        int multiplier = 1000;
        int currentValue = 0;
        xString x1(mapSizeToUSP[2][0]);
        xString x7(mapSizeToUSP[3][0]);
        xString x4(mapSizeToUSP[4][0]);
        xString x6("");
        xString x9("");

        encoder['a'] = detectA(x1,x7);
        encoder['b'] = detectB(x1,mapSizeToUSP[6][0],mapSizeToUSP[6][1],mapSizeToUSP[6][2],x6);
        //Erase x6 for convinience
        for(int i = 0; i < mapSizeToUSP[6].size() ; ++i){
            if (mapSizeToUSP[6][i] == x6){
                mapSizeToUSP[6].erase(mapSizeToUSP[6].begin()+i);
                break;
            }
        }

        xString xAB({encoder['a'],encoder['b']});
        encoder['c'] = detectC(x7,xAB);

        xString xGE(detectG_E(x4,mapSizeToUSP[6][0],mapSizeToUSP[6][1],x9));
        encoder['g'] = xGE[0];
        encoder['e'] = xGE[1];

        xString xBCG({encoder['b'],encoder['c'],encoder['g']});
        encoder['f'] = detectF(xBCG,x4);
        encoder['d'] = detectD(x4,x9,xAB);

        //Create decoder
        for (auto x : encoder){
            decoder[x.second]=x.first; 
        }
        
        for(auto x : helperDOV){
            auto d = getDecodedValue(x,decoder);
            currentValue += (xDigitToInt[d]*multiplier);
            multiplier /= 10;
        }

        totalInput+=currentValue;

    }

    std::cout << "Final sum : " << totalInput << std::endl;

    return 0;
}