#include <iostream>
#include <fstream>
#include <istream>
#include <vector>
#include <map>

constexpr auto DOV = 4; //Digit Output Value
constexpr auto USP = 10; //Unique Signal Pattern

int main(){
    std::string fileName("in8.txt");
    std::fstream file;
    std::vector<std::string> helperUSP;
    std::vector<std::string> helperDOV;
    std::map<std::vector<std::string>,std::vector<std::string>> data;

    int unique = 0;

    file.open(fileName);
    while(!file.eof()){
        std::string helper;
        for(int i = 0 ; i < USP ; i++){
            file >> helper;
            helperUSP.push_back(helper);
        }
        file >> helper; //DROP DELIM
        for(int i = 0 ; i < DOV ;++i){
            file >> helper;
            // std::cout << helper << " size " << helper.size() << std::endl;
            if(helper.size() == 2 || helper.size() == 3 || helper.size() == 4 || helper.size() == 7 ){
                unique++;
            }
            helperDOV.push_back(helper);
        }
    }

    // for (auto x : helperUSP) {
    //         std::cout << x << " " << std::endl;
    //     }
    //     std::cout << std::endl;
    //     for (auto x : helperDOV) {
    //         std::cout << x << " " << std::endl;
    //     }
    //     std::cout << std::endl;

        std::cout << "Unique " << unique << std::endl;
    return 0;
}