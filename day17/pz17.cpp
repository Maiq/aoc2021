#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <map>
#include <vector>
#include <iostream>

#define BUFFSIZE 10
#define MAXSTEPS 1000


struct bounds{
    int32_t xMin;
    int32_t xMax;
    int32_t yMin;
    int32_t yMax;
};

struct stepsAndVelocity{
    int32_t s;
    int32_t v;
};

struct bounds parse(const char* fileName){
    char xMin[BUFFSIZE]={0};
    char xMax[BUFFSIZE]={0};
    char yMin[BUFFSIZE]={0};
    char yMax[BUFFSIZE]={0};
    char* bounds[4] = {xMin,xMax,yMin,yMax};
    struct bounds area = {0};

    int filling = -1;
    int i = 0;
    int commas = 0;
    int equals = 0;
    
    
    FILE* file = fopen(fileName, "r");
    while(1){
        int readChar = getc(file);
        if(readChar == EOF){
            break;
        }
        if(readChar == '='){
            ++equals;
            if(equals == 1){
                filling = 0;
                i = 0;
            }
            if(equals == 2){
                filling = 2;
                i = 0;
            }
            continue;
        }
        if(readChar == '.'){
            ++commas;
            if(commas == 2){
                filling = 1;
                i = 0;
            }
            if(commas == 4){
                filling = 3;
                i = 0;
            }
            continue;
        }
        if(readChar == ','){
            filling = -1;
            continue;
        }
        if(filling != -1){
            bounds[filling][i] = readChar;
            ++i;
        }
    }
    fclose(file);
    area.xMin = atoi(bounds[0]);
    area.xMax = atoi(bounds[1]);
    area.yMin = atoi(bounds[2]);
    area.yMax = atoi(bounds[3]);
    return area;
}

int positionY(int n,int y){
    return (n*y) - ((n*(n+1))/2) + y;
}
int positionX(int n,int x){
    if( n >= abs(x)){
        n = x; //TODO: WHAT IF WE'RE GOING RIGHT?!
    }
    return (- (abs(x)/x)) * (n*(n+1))/2 + (n+1)*x;
}

int findStepsForY(struct bounds target, int * possibleStepsArray,std::map<int,std::vector<int> >& velocityStepMapY){
    int steps[MAXSTEPS] = {0};
    int stepCntr = 0;
    int velocityUp = 0;
    int velocityDown = 0;
    
    int printCntr = 0;
    while(1){
        int n = 1;
        while(1){
            int y = 0;
            int stepTry = (2*velocityUp)+n;
            y = positionY(stepTry, velocityUp);
            if ( y < target.yMin ){
                break;
            }
            if ( target.yMin <= y && y <= target.yMax ){
                // printf("Positon %d speed %d and step %d!\n",y, velocityUp, stepTry);
                printCntr++;
                velocityStepMapY[velocityUp].push_back(stepTry);
                int found = 0;
                for(int j = 0 ; j < stepCntr ; ++j){
                    if(steps[j] == stepTry){
                        found = 1;
                    }
                }
                if(!found){
                    steps[stepCntr] = stepTry;
                    ++stepCntr;
                }
            }
            ++n;
        }
        if ( n == 1){
            break; 
        }
        ++velocityUp;
    }

    while(1){
        int n = 1;
        while(1){
            int y = 0;
            int stepTry = (2*velocityDown)+n;
            y = positionY(stepTry, velocityDown);
            if ( y < target.yMin ){
                break;
            }
            if ( target.yMin <= y && y <= target.yMax ){
                // printf("Positon %d speed %d and step %d!\n",y, velocityDown, stepTry);
                velocityStepMapY[velocityDown].push_back(stepTry);
                printCntr++;                
                int found = 0;

                for(int j = 0 ; j < stepCntr ; ++j){
                    if(steps[j] == stepTry){
                        found = 1;
                    }
                }
                if(!found){
                    steps[stepCntr] = stepTry;
                    ++stepCntr;
                }
            }
            ++n;
        }
        if ( n == 1){
            break; 
        }
        --velocityDown;
    }

    for( auto& pair : velocityStepMapY){
        std::sort(pair.second.begin(),pair.second.end());
    }

    memcpy(possibleStepsArray,steps,sizeof(steps[0])*stepCntr);
    return stepCntr;
}

void findVelocitiesForX(struct bounds target, int * stepsArray,int arrayLen,std::map<int,std::vector<int> >& velocityStepMapX){
int printCntr = 0;
    for( int n = 0; n < arrayLen ; ++n){
        int s = stepsArray[n];
        int vel = 0;
        int min = 0;
        int max = 0;
        int x = 0;
        
        while( min == 0 || max == 00){
            ++vel;
            if( max == 0 ){
                if( target.xMax < 0){
                    max = 1;
                    continue;
                }
                x = positionX(s,vel);
                if( x > target.xMax ){
                    max = 1;
                }
                if ( target.xMin <= x && x <= target.xMax){
                    // printf("Positon %d speed %d and step %d!\n",x, vel, s);
                    printCntr++;
                    velocityStepMapX[vel].push_back(s);
                }
            }
            if( min == 0 ){
                if( target.xMin > 0){
                    min = 1;
                    continue;
                }
                x = positionX(s,-vel);
                if( x > target.xMax ){
                    min = 1;
                }
                if ( target.xMin <= x && x <= target.xMax){
                    // printf("Positon %d speed %d and step %d!\n",x, -vel, s);
                    velocityStepMapX[-vel].push_back(s);
                    printCntr++;
                }
            }
        }
    }
    for( auto& pair : velocityStepMapX){
        std::sort(pair.second.begin(),pair.second.end());
    }
}

auto findVelocitiesAndTime(const std::map<int,std::vector<int> >& velocityStepMapX,const std::map<int,std::vector<int> >& velocityStepMapY){
    std::vector < std::tuple<int,int,int> > combinations_X_Y_STEP;
    for ( auto pairY : velocityStepMapY ){
        for( auto yStep : pairY.second ){
            for(auto pairX : velocityStepMapX){
                if( std::find(pairX.second.begin(),pairX.second.end(),yStep) != pairX.second.end()){
                    combinations_X_Y_STEP.emplace_back(std::make_tuple(pairX.first,pairY.first,yStep));
                }
            }
        }
    }

    return combinations_X_Y_STEP;
}

void findStyle(std::vector < std::tuple<int,int,int> > combinations_X_Y_STEP){
    int highest = 0;
    int y = 0;
    int originX = 0;
    int originY = 0;
    for( auto tuple : combinations_X_Y_STEP){
        int velY = std::get<1>(tuple);
        int velX = std::get<0>(tuple);
        for( int n = 0; n <= std::get<2>(tuple) ; ++n){
            y = positionY(n,velY);
            if (y > highest ){
                highest = y;
                originY = velY;
                originX = velX;
            }
        }
    }
    std::cout << "Heighest point " << highest << " for origin " << originX << "," << originY << std::endl;
}


void outcomes(std::vector<std::tuple<int,int,int> > outcomes){
    auto lambda = [](auto i, auto j)->bool{
        return ((std::get<0>(i) == std::get<0>(j)) && (std::get<1>(i) == std::get<1>(j)) );
    };
    std::sort(outcomes.begin(),outcomes.end());
    auto it = std::unique(outcomes.begin(),outcomes.end(),lambda);
    outcomes.resize(std::distance(outcomes.begin(),it));
    std::cout << "Size outcome " << outcomes.size() << std::endl;
}


int main(){
    int foundSteps[MAXSTEPS] = {0};
    int arrayLen = 0;
    struct bounds target = parse("in17.txt");
    printf("I want to aim to x %d to %d , and y %d to %d\n", target.xMin, target.xMax, target.yMin, target.yMax );
    
    int n = 6;
    int velX = 7;
    int velY = 2;

    int destX = positionX(n,velX);
    int destY = positionY(n, velY);

    std::map<int,std::vector<int>> velocityStepX;
    std::map<int,std::vector<int>> velocityStepY;

    struct bounds example ={20,30,-10,-5}; 

    arrayLen = findStepsForY(target,foundSteps,velocityStepY);


    findVelocitiesForX(target,foundSteps,arrayLen,velocityStepX);
    auto combo = findVelocitiesAndTime(velocityStepX,velocityStepY);
    
    findStyle(combo);
    outcomes(combo);

}