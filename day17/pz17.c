#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFSIZE 10
#define MAXSTEPS 1000

struct bounds{
    int32_t xMin;
    int32_t xMax;
    int32_t yMin;
    int32_t yMax;
};

struct stepsAndVelocity{
    int32_t s;
    int32_t v;
};

struct bounds parse(const char* fileName){
    char xMin[BUFFSIZE]={0};
    char xMax[BUFFSIZE]={0};
    char yMin[BUFFSIZE]={0};
    char yMax[BUFFSIZE]={0};
    char* bounds[4] = {xMin,xMax,yMin,yMax};
    struct bounds area = {0};

    int filling = -1;
    int i = 0;
    int commas = 0;
    int equals = 0;
    
    
    FILE* file = fopen(fileName, "r");
    while(1){
        int readChar = getc(file);
        if(readChar == EOF){
            break;
        }
        if(readChar == '='){
            ++equals;
            if(equals == 1){
                filling = 0;
                i = 0;
            }
            if(equals == 2){
                filling = 2;
                i = 0;
            }
            continue;
        }
        if(readChar == '.'){
            ++commas;
            if(commas == 2){
                filling = 1;
                i = 0;
            }
            if(commas == 4){
                filling = 3;
                i = 0;
            }
            continue;
        }
        if(readChar == ','){
            filling = -1;
            continue;
        }
        if(filling != -1){
            bounds[filling][i] = readChar;
            ++i;
        }
    }
    fclose(file);
    area.xMin = atoi(bounds[0]);
    area.xMax = atoi(bounds[1]);
    area.yMin = atoi(bounds[2]);
    area.yMax = atoi(bounds[3]);
    return area;
}

int positionY(int n,int y){
    return (n*y) - ((n*(n+1))/2) + y;
}
int positionX(int n,int x){
    if( n >= abs(x)){
        n = abs(x); //TODO: WHAT IF WE'RE GOING RIGHT?!
    }
    return (- (abs(x)/x)) * (n*(n+1))/2 + (n+1)*x;
}

int findStepsForY(struct bounds target, int * possibleStepsArray){
    int steps[MAXSTEPS] = {0};
    int stepCntr = 0;
    int foundMin = 0;
    int foundMax = 0;
    int velocityUp = 0;
    int velocityDown = 0;
    

    while(1){
        ++velocityUp;
        int n = 1;
        while(1){
            int y = 0;
            int stepTry = (2*velocityUp)+n;
            y = positionY(stepTry, velocityUp);
            if ( y < target.yMin ){
                break;
            }
            if ( target.yMin <= y && y <= target.yMax ){
                printf("Positon %d speed %d and step %d!\n",y, velocityUp, stepTry);
                int found = 0;
                for(int j = 0 ; j < stepCntr ; ++j){
                    if(steps[j] == stepTry){
                        found = 1;
                    }
                }
                if(!found){
                    steps[stepCntr] = stepTry;
                    ++stepCntr;
                }
            }
            ++n;
        }
        if ( n == 1){
            break; 
        }
    }

    while(1){
        --velocityDown;
        int n = 1;
        while(1){
            int y = 0;
            int stepTry = (2*velocityDown)+n;
            y = positionY(stepTry, velocityDown);
            if ( y < target.yMin ){
                break;
            }
            if ( target.yMin <= y && y <= target.yMax ){
                printf("Positon %d speed %d and step %d!\n",y, velocityDown, stepTry);
                                int found = 0;
                for(int j = 0 ; j < stepCntr ; ++j){
                    if(steps[j] == stepTry){
                        found = 1;
                    }
                }
                if(!found){
                    steps[stepCntr] = stepTry;
                    ++stepCntr;
                }
            }
            ++n;
        }
        if ( n == 1){
            break; 
        }
    }

    memcpy(possibleStepsArray,steps,sizeof(steps[0])*stepCntr);
    return stepCntr;
}

void findVelocitiesForX(struct bounds target, int * stepsArray,int arrayLen){

    for( int n = 0; n < arrayLen ; ++n){
        int s = stepsArray[n];
        int vel = 0;
        int min = 0;
        int max = 0;
        int x = 0;
        while( min == 0 || max == 00){
            ++vel;
            if( max == 0 ){
                x = positionX(s,vel);
                if( x > target.xMax ){
                    max = 1;
                }
                if ( target.xMin <= x && x <= target.xMax){
                    printf("Positon %d speed %d and step %d!\n",x, vel, s);
                }
            }
            if( min == 0 ){
                x = positionX(s,-vel);
                if( x > target.xMax ){
                    min = 1;
                }
                if ( target.xMin <= x && x <= target.xMax){
                    printf("Positon %d speed %d and step %d!\n",x, -vel, s);
                }
            }
        }
    }
}



int main(){
    int foundSteps[MAXSTEPS] = {0};
    int arrayLen = 0;
    struct bounds target = parse("in17.txt");
    printf("I want to aim to x %d to %d , and y %d to %d\n", target.xMin, target.xMax, target.yMin, target.yMax );
    
    int n = 6;
    int velX = 7;
    int velY = 2;

    int destX = positionX(n,velX);
    int destY = positionY(n, velY);

    printf("Destination %d,%d\n",destX, destY);

    struct bounds example ={20,30,-10,-5}; 

    arrayLen = findStepsForY(example,foundSteps);

    for ( int k = 0 ; k < arrayLen ; ++k){
        printf("Good step %d\n",foundSteps[k]);
    }

    findVelocitiesForX(example,foundSteps,arrayLen);

}