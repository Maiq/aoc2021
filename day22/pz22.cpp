#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>
#include <deque>
#include <list>
#include <fstream>

class Range{
    public:
    Range(long long int b,long long int t):bot(b),top(t){};

    bool overlap(const Range& other)
    {
        if(other.getTop() < bot){
            return false;
        }
        if(other.getBot() > top){
            return false;
        }
        return true;
    }
    bool operator==(const Range& other){
        return other.getBot() == bot && other.getTop() == top;
    }
    bool operator!=(const Range& other){
        return !(*this == other);
    }

    std::list<Range> split(const Range& other){
        std::vector<int> p;
        std::list<Range> outcome;
        p.push_back(bot);
        p.push_back(top);
        p.push_back(other.getBot());
        p.push_back(other.getTop());
        std::sort(p.begin(),p.end());

        if(p[0] <= p[1]-1){
            outcome.push_back(Range(p[0],p[1]-1));
        }
        outcome.push_back(Range(p[1],p[2]));
        if(p[2]+1 <= p[3]){
            outcome.push_back(Range(p[2]+1,p[3]));
        }

        return outcome;
    }

    long long int getBot() const {return bot;};
    long long int getTop() const {return top;};

    private:
    long long int bot;
    long long int top;
};

class Cuboid{
    public:
    Cuboid(Range x, Range y, Range z):X(x),Y(y),Z(z){};

    bool overlap(Cuboid& other){
        return X.overlap(other.getX()) && Y.overlap(other.getY()) && Z.overlap(other.getZ());
    }

    std::list<Cuboid> deconstruct(Cuboid &other){
        std::list<Cuboid> outcome;
        auto newX = X.split(other.getX());
        auto newY = Y.split(other.getY());
        auto newZ = Z.split(other.getZ());
        for(auto& x : newX){
            for(auto& y : newY){
                for(auto& z : newZ){
                    auto C = Cuboid(x,y,z);
                    if(!other.overlap(C) && overlap(C)){
                        outcome.push_back(C);
                    }
                }
            }
        }
        return outcome;
    }

    Cuboid intersection(Cuboid& other){
        auto newX = X.split(other.getX());
        auto newY = Y.split(other.getY());
        auto newZ = Z.split(other.getZ());
        for(auto& x : newX){
            for(auto& y : newY){
                for(auto& z : newZ){
                    auto C = Cuboid(x,y,z);
                    if(other.overlap(C) && overlap(C)){
                        return C;
                    }
                }
            }
        }
    }

    Range getX(){return X;};
    Range getY(){return Y;};
    Range getZ(){return Z;};

    long long int getVolume(){
        return ((X.getTop()-X.getBot()) + 1) * ((Y.getTop()-Y.getBot()) + 1) * ((Z.getTop()-Z.getBot()) + 1);
    }
    
    void print(){
        std::cout << "X:" << X.getBot() << ";" << X.getTop() << std::endl;
        std::cout << "Y:" << Y.getBot() << ";" << Y.getTop() << std::endl;
        std::cout << "Z:" << Z.getBot() << ";" << Z.getTop() << std::endl;
    }

    void debug(){   
        std::cout <<    "on x=" << X.getBot() << ".." << X.getTop() << \
                        ",y=" << Y.getBot() << ".." << Y.getTop() << \
                        ",z=" << Z.getBot() << ".." << Z.getTop() << std::endl;
    }

    private:
    Range X;
    Range Y;
    Range Z;

};

std::list<Cuboid> parse(std::string filename,long long int limit=0){
    std::fstream file(filename);
    std::string buff;
    std::vector<Range> c;
    std::list<Cuboid> outcome;
    while(!file.eof()){
        std::getline(file,buff);
        bool off = false;
        c.clear();
        if( buff.find("off") != std::string::npos){
            off = true;
        }
        for(long long int i = 0 ; i < 3 ;++i){
            auto eq = buff.find("=");
            auto cm = buff.find(",");
            auto dot = buff.find(".");
            auto lower = std::stoi(buff.substr(eq+1,dot-eq-1));
            auto upper = std::stoi(buff.substr(dot+2,cm-dot-2));
            buff = buff.substr(cm+1);
            c.push_back(Range(lower,upper));
        }
        if(limit != 0){
            if( !(c[0].getBot() >= -limit && c[0].getTop() <= limit &&\
                  c[1].getBot() >= -limit && c[1].getTop() <= limit &&\
                  c[2].getBot() >= -limit && c[2].getTop() <= limit))
                {
                    continue;
                }
        }
        Cuboid C(c[0],c[1],c[2]);
        if(off){
            //TODO:deconstruct;
            std::list<Cuboid> deconstructed;
            for(auto& current : outcome){
                std::cout<<"Original: " << std::endl;
                current.print();
                if(current.overlap(C)){
                    auto tmp = current.deconstruct(C);
                    deconstructed.insert(deconstructed.end(),tmp.begin(),tmp.end());
                }
                else{
                    deconstructed.push_back(current);
                }
                std::cout<<"Deconstructed: " << std::endl;
                for(auto& d : deconstructed){
                    d.debug();
                }
            }
            outcome = deconstructed;
        }
        else{
            outcome.push_back(C);
        }
    }
    file.close();
    return outcome;
}

long long int IEP(Cuboid& A, long long int i, long long int depth, std::vector<Cuboid>& cores){
    static long long int counter = 0;
    long long int n = depth%2 == 0 ? 1 : -1;
    counter += n * A.getVolume();
    std::cout <<" Current counter " << counter << std::endl;
    while(i < cores.size()){
        if(A.overlap(cores[i])){
            Cuboid intersected = A.intersection(cores[i]);
            IEP(intersected,i+1, depth+1,cores);
        }
        else{
            // return counter;
        }
        ++i;
    }
    return counter;
}

int main(){
    Cuboid A(Range(10,12),Range(10,12),Range(10,12));
    Cuboid C(Range(11,13),Range(11,13),Range(11,13));

    // if(A.overlap(C)){
    //     auto V = A.deconstruct(C);
    //     for(auto& v : V){
    //         v.print();
    //         std::cout << std::endl;
    //     }
    // }
    auto V = parse("pz22.txt");
    // for(auto& v : V){
    //         v.print();
    //         std::cout << std::endl;
    // }
    std::vector<Cuboid> T;
    
    // T.push_back(A);
    // T.push_back(C);
    T.insert(T.end(),V.begin(),V.end());
    long long int v;
    for(long long int i = 0; i< T.size(); ++i){
        long long int j = i+1;
        v = IEP(T[i],j,0,T);
    }
    
    

    std::cout << "Volume: " << v << std::endl;
}