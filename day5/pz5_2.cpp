#include <iostream>
#include <regex>
#include <utility>
#include <fstream>
#include <cstdlib>
#include <cmath>

constexpr auto OVERLAP = 2;

class Point{
    public:
        Point(int x, int y):m_x(x),m_y(y){};
        int X(){return m_x;};
        int Y(){return m_y;};
    private:
        int m_x;
        int m_y;
};

class Segment{
    public:
        Segment(Point A, Point B):m_A(A),m_B(B){

            //A WILL BE ALWAYS CLOSER TO (0,0);
            int a = (A.X()*A.X()) + (A.Y()*A.Y());
            int b = (B.X()*B.X()) + (B.Y()*B.Y());

            if( a < b ){
                m_A = A;
                m_B = B;
            }else{
                m_A = B;
                m_B = A;
            }
            m_furthestCoordinate = ( m_B.X() > m_B.Y() ? m_B.X() : m_B.Y());
        };
        Point A() const {return m_A;};
        Point B() const {return m_B;};
        int getFurthest() const {
            return m_furthestCoordinate;
        };

        int getManhattanDistance() const {
            return ( abs(A().X() - B().X()) + abs(A().Y() - B().Y()) ) ;
        }

        int getCeilEuclidean() const {
            
        }
    
    private:
    Point m_A;
    Point m_B;
    int m_furthestCoordinate;
};

class Diagram{
    public:
        Diagram(int diagramSize){
            std::vector<int> helper;
            for(int n = 0 ; n < diagramSize ; ++n){
                helper.push_back(0);
            }
            for(int i = 0 ; i < diagramSize ; i++){
                m_diag.push_back(helper);
            }
            
        };
        void applySegment(const Segment&& seg){
             if( seg.getFurthest() > getSize() ){
                 resize(seg.getFurthest());
             }
             if( seg.A().X() == seg.B().X() ) {
                //FILL VERTICAL
                for( int d = 0 ; d <= seg.getManhattanDistance() ; ++d){
                    m_diag[seg.A().Y() + d][seg.A().X() ] += 1;
                }
                
             }
             else if ( seg.A().Y() == seg.B().Y() ) {
                 for( int d = 0 ; d <= seg.getManhattanDistance() ; ++d){
                    //FILL HORIZONTAL
                    m_diag[seg.A().Y()][seg.A().X() + d] += 1;
                    
                }
             }
             else{
                 //FILL DIAGONAL
                 int x = seg.A().X();
                 int y = seg.A().Y();
                 int count = seg.getManhattanDistance()/2;
                 
                 if( seg.A().X() > seg.B().X() && seg.A().Y() > seg.B().Y() ){
                    for ( int i = 0 ; i <= count ; ++i){
                            m_diag[ y - i ][ x - i ] += 1;
                        }
                 }
                 else if( seg.A().X() > seg.B().X() && seg.A().Y() < seg.B().Y() ){
                    for ( int i = 0 ; i <= count ; ++i){
                            m_diag[ y + i ][ x - i ] += 1;
                        }
                 }            
                 else if ( seg.A().X() < seg.B().X() && seg.A().Y() < seg.B().Y() ){
                    for ( int i = 0 ; i <= count ; ++i){
                            m_diag[ y + i ][ x + i ] += 1;
                        }
                 }     
                 else {
                    for ( int i = 0 ; i <= count ; ++i){
                            m_diag[ y - i ][ x + i ] += 1;
                        }
                 }
                 
             }
             
        }

        int getSize(){
            return m_diag.size();
        }

        void resize(int newSize){
            newSize += 1;
            int resizeValue = newSize - getSize();
            std::vector<int> helper;
            //Resize rows;
            for( int i = 0 ; i < resizeValue ; ++i){
                helper.push_back(0);
            }
            for(auto& row : m_diag){
                row.insert(row.end(),helper.begin(),helper.end());
            }

            while(helper.size() < newSize){
                helper.push_back(0);
            }
            for(int i = 0 ; i < resizeValue ; ++i){
                m_diag.push_back(helper);
            }
        };

        int getDangerSpots(){
            int dangerSpots = 0;
            for ( auto x : m_diag){
                for (auto y : x){
                    if( y >= OVERLAP ){
                        ++dangerSpots;
                    }
                }
            }
            std::cout << " DANGER SPOTS: " << dangerSpots << std::endl;
            return dangerSpots;
        }

        void print(){
            for(auto x : m_diag){
                for(auto y : x){
                    std::cout << y ;
                }
                std::cout << std::endl;
            }
        }
        private:
        std::vector< std::vector<int> > m_diag;
};

Segment stringToSegment(const std::string& stringToParse){
    std::regex re(R"((\d+),(\d+)\s*->\s*(\d+),(\d+))");
    std::smatch matches;

    std::regex_search (stringToParse, matches, re);
    Point A(std::stoi(matches[1]),std::stoi(matches[2]));
    Point B(std::stoi(matches[3]),std::stoi(matches[4]));

    return Segment(A,B);

}


int main(){
    std::string fileName("in5.txt");

    Diagram d(10);
    int i = 0;
    
    std::fstream file;
    file.open(fileName,std::fstream::in);
    while(!file.eof()){
        std::string inputLine;
        std::getline(file,inputLine);
        Segment s = stringToSegment(inputLine);
        // std:: cout << s.A().X() << " " << s.A().Y () << " - " << s.B().X() << " " << s.B().Y ()  << std::endl; 
        d.applySegment(stringToSegment(inputLine));
    }
    // d.print();
    d.getDangerSpots();
    return 0;
}