#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <stack>
#include <vector>
#include <algorithm>
const std::string opening("([{<");
const std::string closing(")]}>");

int main(){
    std::string fileName("in10.txt");
    std::fstream file;
    char character;
    bool syntaxError = false;
    std::string line;
    std::stack<char> oStack;
    std::map<char, int> syntaxErrorCount { {')',0} ,{']',0},{'}',0},{'>',0}};
    std::map<char, int> syntaxErrorValue { {')',3} ,{']',57},{'}',1197},{'>',25137}};
    std::map<char, int> autoCompleteValue { {')',1} ,{']',2},{'}',3},{'>',4}};
    std::vector<long unsigned int> autoCompleteScores;
    std::string autoComplete = "";
    file.open(fileName);
    while(!file.eof()){
        std::getline(file,line);
        syntaxError = false;
        autoComplete = "";
        for( auto c : line){
            auto closePos = closing.find(c);
            if( closePos == std::string::npos){
                oStack.push(c);
            }else{
                auto openPos = opening.find(oStack.top());
                if( openPos == closePos){
                    oStack.pop();
                }
                else{
                    // std::cout << "Corrupted! Expected " << closing[openPos] << " but found  " << c << " instead"<<std::endl;
                    syntaxErrorCount[c] += 1;
                    syntaxError = true;
                    while(oStack.size()){
                        oStack.pop();
                    }
                    break;
                }
            }
        }
        if(!syntaxError){
            while(oStack.size()){
                auto openPos = opening.find(oStack.top());
                autoComplete.push_back(closing[openPos]);
                oStack.pop();
            }
            // std::cout << "Auto complete " << autoComplete << std::endl;
            long unsigned int autoCompleteScore = 0;
            for (auto c : autoComplete){
                autoCompleteScore *= 5;
                autoCompleteScore += autoCompleteValue[c];
            }
            autoCompleteScores.push_back(autoCompleteScore);
            
        }
    }
    int totalErrorScore = 0;
    for (auto pair : syntaxErrorCount){
        totalErrorScore += (syntaxErrorValue[pair.first] * pair.second);
    }
    std::cout << "Total error score " << totalErrorScore << std::endl;
    std::sort(autoCompleteScores.begin(),autoCompleteScores.end());
    std::cout << "Middle auto complete score : " << autoCompleteScores[(autoCompleteScores.size()/2)] << std::endl;


    return 0;
}