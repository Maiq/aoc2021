import java.io.File

fun getLines() = File("in10.txt").readLines()

val syntax: Map<Char,Char> = mapOf( ')' to '(', ']' to '[' , '}' to '{' , '>' to '<' )
val errorScore: Map<Char,Int> = mapOf( ')' to 3, ']' to 57 , '}' to 1197 , '>' to 25137 )
val autoCompleteScore: Map<Char,Int> = mapOf( '(' to 1, '[' to 2, '{' to 3, '<' to 4)
var totalScoreList: MutableList<Long> = mutableListOf()
fun main(){

    var lines = getLines()
    var cmd:String
    var errorValue = 0
    var totalScore:Long
    for ( line in lines ){
        totalScore = 0;
        cmd = ""
        for( char in line ){
            if( !syntax.containsKey(char) ){
                cmd+=char
            }
            else{
                if(cmd.last() == syntax[char]){
                    cmd = cmd.dropLast(1)
                }else{
                    // println("Syntax error in line $line")
                    errorValue = errorValue + (errorScore[char]!!)
                    cmd = ""
                    break;
                }
            }
        }
        if (cmd.isEmpty()){
            continue;
        }
        cmd = cmd.reversed();
        for ( char in cmd){
            totalScore = totalScore * 5
            totalScore = totalScore + (autoCompleteScore[char]!!)
        }
        totalScoreList.add(totalScore)
    }
    totalScoreList.sort()
    var part2 = totalScoreList[totalScoreList.size/2]
    println("Error score $errorValue")
    println("AutoComplete score $part2")
}