#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define ASCII_0 48


void allocBuffer(void** buffer,uint32_t length,size_t size){
    *buffer = calloc(length*size,size);
}

// Data structure to represent a stack
struct stack
{
    int maxsize;    // define max capacity of the stack
    int top;
    int *items;
};
 
// Utility function to initialize the stack
struct stack* newStack(int capacity)
{
    struct stack *pt = (struct stack*)malloc(sizeof(struct stack));
 
    pt->maxsize = capacity;
    pt->top = -1;
    pt->items = (int*)malloc(sizeof(int) * capacity);
 
    return pt;
}
 
// Utility function to return the size of the stack
int size(struct stack *pt) {
    return pt->top + 1;
}
 
// Utility function to check if the stack is empty or not
int isEmpty(struct stack *pt) {
    return pt->top == -1;                   // or return size(pt) == 0;
}
 
// Utility function to check if the stack is full or not
int isFull(struct stack *pt) {
    return pt->top == pt->maxsize - 1;      // or return size(pt) == pt->maxsize;
}
 
// Utility function to add an element `x` to the stack
void push(struct stack *pt, int x)
{
    // check if the stack is already full. Then inserting an element would
    // lead to stack overflow
    if (isFull(pt))
    {
        printf("Overflow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }
 
    // printf("Inserting %d\n", x);
 
    // add an element and increment the top's index
    pt->items[++pt->top] = x;
}
 
// Utility function to return the top element of the stack
int peek(struct stack *pt)
{
    // check for an empty stack
    if (!isEmpty(pt)) {
        return pt->items[pt->top];
    }
    else {
        exit(EXIT_FAILURE);
    }
}
 
// Utility function to pop a top element from the stack
int pop(struct stack *pt)
{
    // check for stack underflow
    if (isEmpty(pt))
    {
        printf("Underflow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }
 
    // printf("Removing %d\n", peek(pt));
 
    // decrement stack size by 1 and (optionally) return the popped element
    return pt->items[pt->top--];
}


uint32_t convertStringToInt(char* buff,uint32_t len){
    uint32_t binary = 0;
    for(int i = 0 ; i < len ; i++){
        binary = binary << 1;
        if ( buff[i] != '0'){
            binary += 1;
        }
    }
    return binary;
}


int main(){
    
    char* buffer = NULL;
    int32_t* gammaRate;
    int32_t* epsilonRate;    

    int32_t gammaRateValue = 0;
    int32_t epsilonRateValue = 0;

    const char * fileName = "in3.txt";
    FILE* file = fopen(fileName, "r");
    char lf = '\0';
    uint8_t counter = 0;
    uint8_t dataLengthWithLF;
    uint32_t readout = 0;
    while(lf != '\n'){
        fread(&lf,sizeof(lf),sizeof(lf),file);
        ++counter; // determine the size of data.
    }
    fclose(file); //reset file
    file = fopen(fileName,"r");

    dataLengthWithLF = counter*sizeof(lf);

    allocBuffer((void*)&buffer,dataLengthWithLF,sizeof(char));
    allocBuffer((void*)&gammaRate,dataLengthWithLF-1,sizeof(int32_t)); //alloc without '\n'
    allocBuffer((void*)&epsilonRate,dataLengthWithLF-1,sizeof(int32_t)); //alloc without '\n'
    

    while(fread(buffer,sizeof(lf),dataLengthWithLF,file) == dataLengthWithLF){
        ++readout;
        for(counter = 0 ; counter < dataLengthWithLF - 1 ; ++counter){
            // printf("%c",buffer[counter]);
            if( (buffer[counter]-ASCII_0) ) { // 1
                gammaRate[counter]++;
                epsilonRate[counter]--;
            }else{ // 0
                gammaRate[counter]--;
                epsilonRate[counter]++;
            }
        }        
        // printf("\n");
    }
    // printf("GammaRate Counter[0] %d",gammaRate[0]);

    for(counter = 0 ; counter < dataLengthWithLF -1 ; ++counter){
        gammaRateValue = ( gammaRateValue << 1 );
        epsilonRateValue = (epsilonRateValue << 1 );
        if(gammaRate[counter] > 0){
            ++gammaRateValue;
        }
        if(epsilonRate[counter] > 0){
            ++epsilonRateValue;
        }
    }
    printf("Gamma rate value %d\n",gammaRateValue);
    printf("Epsilon rate value %d\n",epsilonRateValue);
    printf("Power consumption %d\n",(epsilonRateValue*gammaRateValue));

    fclose(file);
    ///////////PART 2


    struct stack *oxygenStackA = newStack(readout);
    struct stack *oxygenStackB = newStack(readout);
    struct stack *CO2StackA = newStack(readout);
    struct stack *CO2StackB = newStack(readout);


    file = fopen(fileName, "r");
    while(fread(buffer,sizeof(lf),dataLengthWithLF,file) == dataLengthWithLF){
        if( (buffer[0]-ASCII_0) == (gammaRateValue >> (dataLengthWithLF - 2)) ) { // IS THIS BIT SHIFT PROPER?
            //Bit criteria correct            
            push(oxygenStackA,convertStringToInt(buffer,dataLengthWithLF-1));
        }
        if( (buffer[0]-ASCII_0) == (epsilonRateValue >> (dataLengthWithLF - 2) ) ) { // IS THIS BIT SHIFT PROPER?
            //Bit criteria correct
            push(CO2StackA,convertStringToInt(buffer,dataLengthWithLF-1));
        }
    }

    for( int bitwise = 3 ; bitwise <= dataLengthWithLF; ++bitwise){
        
        uint32_t mask = 1 << dataLengthWithLF - bitwise;
        printf("MASKA %d\n", mask);
        uint32_t oneCounter = 0;
        uint32_t zeroCounter = 0;

        while( !isEmpty(oxygenStackA)) {
            
            uint32_t value = pop(oxygenStackA);
            printf(" VALUE : %d \n", ( value & mask));
            if( value & mask){
                oneCounter++;
            }else{
                zeroCounter++;
            }
            push(oxygenStackB,value);        
        }
        printf(" ONCE : %d - ZEROS %d \n", oneCounter, zeroCounter);
        while( !isEmpty(oxygenStackB)) {
            uint32_t value = pop(oxygenStackB);
            if( zeroCounter > oneCounter ){
                //Zero na tym bicie ktory rozwazam
                if( !(value & mask) ){
                    push(oxygenStackA,value);
                }
            }else{
                if( (value & mask )){
                    push(oxygenStackA,value);
                }
            }
        }

        if(size(oxygenStackA) == 1){
            break;
        }
    }


  for( int bitwise = 3 ; bitwise <= dataLengthWithLF; ++bitwise){
        
        uint32_t mask = 1 << dataLengthWithLF - bitwise;
        uint32_t oneCounter = 0;
        uint32_t zeroCounter = 0;

        while( !isEmpty(CO2StackA)) {
            
            uint32_t value = pop(CO2StackA);
            if( value & mask){
                oneCounter++;
            }else{
                zeroCounter++;
            }
            push(CO2StackB,value);        
        }
        while( !isEmpty(CO2StackB)) {
            uint32_t value = pop(CO2StackB);
            if( zeroCounter <= oneCounter ){
                //Zero na tym bicie ktory rozwazam
                if( !(value & mask) ){
                    push(CO2StackA,value);
                }
            }else{
                if( (value & mask )){
                    push(CO2StackA,value);
                }
            }
        }

        if(size(CO2StackA) == 1){
            break;
        }
    }

    uint32_t oxygenValue = pop(oxygenStackA);
    uint32_t co2Value = pop(CO2StackA);
    printf("Oxygen value: %d\n", oxygenValue);
    printf("CO2 value: %d\n", co2Value);
    printf("Life support rating %d\n", (oxygenValue*co2Value));
    free(buffer);
    free(gammaRate);
    free(epsilonRate);
    return 0;
}