#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define ASCII_0 48

void allocBuffer(void** buffer,uint32_t length,size_t size){
    *buffer = calloc(length*size,size);
}


int main(){
    
    char* buffer = NULL;
    int32_t* gammaRate;
    int32_t* epsilonRate;    

    int32_t gammaRateValue = 0;
    int32_t epsilonRateValue = 0;

    const char * fileName = "in3.txt";
    FILE* file = fopen(fileName, "r");
    char lf = '\0';
    uint8_t counter = 0;
    uint8_t dataLengthWithLF;
    uint32_t readout = 0;
    while(lf != '\n'){
        fread(&lf,sizeof(lf),sizeof(lf),file);
        ++counter; // determine the size of data.
    }
    fclose(file); //reset file
    file = fopen(fileName,"r");

    dataLengthWithLF = counter*sizeof(lf);

    allocBuffer((void*)&buffer,dataLengthWithLF,sizeof(char));
    allocBuffer((void*)&gammaRate,dataLengthWithLF-1,sizeof(int32_t)); //alloc without '\n'
    allocBuffer((void*)&epsilonRate,dataLengthWithLF-1,sizeof(int32_t)); //alloc without '\n'
    

    while(fread(buffer,sizeof(lf),dataLengthWithLF,file) == dataLengthWithLF){
        for(counter = 0 ; counter < dataLengthWithLF - 1 ; ++counter){
            printf("%c",buffer[counter]);
            if( (buffer[counter]-ASCII_0) ) { // 1
                gammaRate[counter]++;
                epsilonRate[counter]--;
            }else{ // 0
                gammaRate[counter]--;
                epsilonRate[counter]++;
            }
        }
        printf("\n");
    }
    printf("GammaRate Counter[0] %d",gammaRate[0]);

    for(counter = 0 ; counter < dataLengthWithLF -1 ; ++counter){
        gammaRateValue = ( gammaRateValue << 1 );
        epsilonRateValue = (epsilonRateValue << 1 );
        if(gammaRate[counter] > 0){
            ++gammaRateValue;
        }
        if(epsilonRate[counter] > 0){
            ++epsilonRateValue;
        }
    }

    printf("Gamma rate value %d\n",gammaRateValue);
    printf("Epsilon rate value %d\n",epsilonRateValue);
    printf("Power consumption %d\n",(epsilonRateValue*gammaRateValue));

    



    
    free(buffer);
    free(gammaRate);
    free(epsilonRate);
    return 0;
}