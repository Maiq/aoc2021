from operator import add

foldInstruction = []
maxX = 10
maxY = 10
paper = [['.']*maxX]*maxY
foldSet = []

def expandPaper(xExpandBy,yExpandBy):
    if(xExpandBy > 0):
        for i in range(0,len(paper)):
            paper[i] = paper[i] + ['.']*xExpandBy
    if(yExpandBy > 0):
        for i in range(0,yExpandBy):
            paper.append(['.']*len(paper[0]))

def printPaper():
    for row in paper:
        printable = ''.join(map(str,row))
        printable = printable.replace('.',' ')
        print(printable)
        

def foldY(n):
    newPaper = paper
    if n < int(len(paper)/2):
        print ("ASSERT Y")
        exit(0)
    
    for i in range (1, (len(paper)-n)):
        r1 = paper[n-i]
        r2 = paper[n+i]
        foldedRow = list(map(add,r1,r2))
        newPaper[n-i] = foldedRow

    newPaper = newPaper[0:n]    
    for y in range(0,len(newPaper)):
        for x in range(0,len(newPaper[0])):
            newPaper[y][x] = '#' if newPaper[y][x].find("#") != -1 else '.'
    return newPaper

def foldX(n):
    newPaper = paper
    if n < int(len(paper[0])/2):
        print( "ASSERT X")
        exit(0)
    
    for y in range(0, len(paper)):
        toAdd = paper[y][n+1:]
        for i in range(0,len(toAdd)):
            newPaper[y][n-1-i] += toAdd[i]
        newPaper[y] = newPaper[y][0:n]
    
    for y in range(0,len(newPaper)):
        for x in range(0,len(newPaper[0])):
            newPaper[y][x] = '#' if newPaper[y][x].find("#") != -1 else '.'
    return newPaper
   

def fold(step):
    axis = step[0]
    foldLine = int(step[1])
    if axis == 'y':
        return foldY(foldLine)
    elif axis == 'x':
        return foldX(foldLine)
    else:
        print("Axis error")
        exit(0)
        

def parse():
    with open("in13.txt") as f:
        for line in f.readlines():
            if(line.find("fold")):
                if(line=="\n"):
                    continue
                point = (line.strip().split(','))
                point = tuple(map(int,point))
                x = point[0]
                y = point[1]
                expandPaper(x - len(paper[0])+1, y - len(paper)+1)
                paper[y][x] = "#"                      
            else:
                foldSet.append(tuple(line.strip().split()[-1].split('=')))


__name__== '__main__'
parse()

'''
#PART1
print ("_____")
paper = fold(foldSet[0])
printPaper()
counter = 0
for x in paper:
    for y in x:
        if y == "#":
            counter +=1
print(counter)
'''


#PART2
for instruction in foldSet:
    paper = fold(instruction)
printPaper()


