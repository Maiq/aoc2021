#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <list>
#include <cctype>
#include <vector>

const std::string START = "start";
const std::string END = "end";
const std::string ORIGIN = "";
const std::string VISITED = "VISIT";
long int totalPathCounter = 0;
std::vector< std::list<std::string> > paths;

void parse(std::map< std::string,std::list<std::string> >& G){
    const std::string fileName("in12.txt");
    std::fstream file;
    std::string input;
    file.open(fileName);
    while(!file.eof()){
        std::getline(file,input);
        auto pos = input.find('-');
        auto a = input.substr(0,pos);
        auto b = input.substr(pos+1,input.length());
        G[a].push_back(b);
        G[b].push_back(a); 
    }
}
void prepareForDFS(std::map< std::string,std::list<std::string> >& G){
    for(auto v : G["start"]){
        G[v].remove_if([](const std::string& v){return (v=="start");});
    }
}
bool isSmallCave(const std::string& s){
    if(s == START || s == END){
        return false;
    }
    for(const auto& c : s){
        if(std::islower(static_cast<unsigned char>(c))){
            return true;
        }
    }
    return false;
}

bool canVisit(const std::string& nextCave, std::list<std::string>& path){
    std::map<std::string, int> occurance;
    bool result = true;
    for(auto x : path){
        // std::cout << x << " - ";
        if(isSmallCave(x)){
            occurance[x] +=1;
        }
    }
    // std::cout << std::endl;
    for(auto x : occurance){
        // std::cout << x.first << " occured " << x.second << " times" << std::endl;
        if(x.second > 1){
            result = false;
        }
    }
    return result;
}

void DFS(std::map< std::string,std::list<std::string> >& G,const std::string& start, std::list<std::string>& path){
    static int recursiveLock = 0;
    path.push_back(start);
    if(start == "end"){
        paths.push_back(path);
        ++totalPathCounter;
        return;
    }
    for(const auto& nextCave : G[start]){
        if(isSmallCave(nextCave) && (std::find(path.begin(),path.end(),nextCave)!=path.end())){
            if(!canVisit(nextCave,path)){
                continue;
            }
        }
        DFS(G,nextCave,path);
        path.pop_back();
    }

}



int main(){
    std::map < std::string, std::list<std::string> > graph;
    std::list< std::string > path;
    parse(graph);
    prepareForDFS(graph);
    for(auto vert : graph){
        std::cout << vert.first << " :" ; 
        for(auto x : vert.second){
            std::cout << " " << x ;
        }
        std::cout /*<< " Size : " << vert.second.size()*/ << std::endl;
    }
    
    DFS(graph,START,path);
    
    for(auto p : paths){
        // std::cout << " Path : " ;
        for (auto v : p){
            // std::cout << " " << v ;
        }
        // std::cout << std::endl;
    }
    std::cout << "Size is " << paths.size() << std::endl;
    std::cout << "Size is " << totalPathCounter << std::endl;
}