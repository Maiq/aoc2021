#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <list>
#include <cctype>
#include <vector>

const std::string START = "start";
const std::string END = "end";
const std::string ORIGIN = "";
std::vector< std::list<std::string> > paths;

void parse(std::map< std::string,std::list<std::string> >& G){
    const std::string fileName("in12.txt");
    std::fstream file;
    std::string input;
    file.open(fileName);
    while(!file.eof()){
        std::getline(file,input);
        auto pos = input.find('-');
        auto a = input.substr(0,pos);
        auto b = input.substr(pos+1,input.length());
        G[a].push_back(b);
        G[b].push_back(a); 
    }
}
void prepareForDFS(std::map< std::string,std::list<std::string> >& G){
    for(auto v : G["start"]){
        G[v].remove_if([](const std::string& v){return (v=="start");});
    }
}
bool isSmallCave(const std::string& s){
    if(s == START || s == END){
        return false;
    }
    for(const auto& c : s){
        if(std::islower(static_cast<unsigned char>(c))){
            return true;
        }
    }
    return false;
}

void DFS(std::map< std::string,std::list<std::string> >& G,const std::string& start, std::list<std::string>& path){
    static int recursiveLock = 0;
    path.push_back(start);
    if(start == "end"){
        paths.push_back(path);
        return;
    }
    for(const auto& nextCave : G[start]){
        if(isSmallCave(nextCave) && (std::find(path.begin(),path.end(),nextCave)!=path.end())){
            if(recursiveLock != 0 ){// Change to TRUE for Part1
                continue;
            }
            else{
                recursiveLock = path.size();
            }
            
        }
        DFS(G,nextCave,path);
        path.pop_back();
        if(path.size() == recursiveLock){
            recursiveLock = 0;
        }
    }

}


int main(){
    std::map < std::string, std::list<std::string> > graph;
    std::list< std::string > path;
    parse(graph);
    prepareForDFS(graph);
    for(auto vert : graph){
        std::cout << vert.first << " :" ; 
        for(auto x : vert.second){
            std::cout << " " << x ;
        }
        std::cout /*<< " Size : " << vert.second.size()*/ << std::endl;
    }
    
    DFS(graph,START,path);
    
    // for(auto p : paths){
    //     std::cout << " Path : " ;
    //     for (auto v : p){
    //         std::cout << " " << v ;
    //     }
    //     std::cout << std::endl;
    // }
    std::cout << "Size is " << paths.size() << std::endl;
}