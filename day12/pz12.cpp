#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <list>
#include <cctype>
#include <vector>

const std::string START = "start";
const std::string ORIGIN = "";
const std::string VISITED = "VISIT";
long int totalPathCounter = 0;
std::vector< std::list<std::string> > paths;

void parse(std::map< std::string,std::list<std::string> >& G){
    const std::string fileName("in12.txt");
    std::fstream file;
    std::string input;
    file.open(fileName);
    while(!file.eof()){
        std::getline(file,input);
        auto pos = input.find('-');
        auto a = input.substr(0,pos);
        auto b = input.substr(pos+1,input.length());
        G[a].push_back(b);
        G[b].push_back(a); 
    }
}
void prepareForDFS(std::map< std::string,std::list<std::string> >& G){
    for(auto v : G["start"]){
        G[v].remove_if([](const std::string& v){return (v=="start");});
    }
}
bool isSmallCave(const std::string& s){
    for(const auto& c : s){
        if(std::islower(static_cast<unsigned char>(c))){
            return true;
        }
    }
    return false;
}
bool canVisit(const std::string& s, std::list<std::string>& path){
    if(isSmallCave(s)){
        if(path.front() == VISITED){
            return false;
        }
        else{
            path.push_front(VISITED);
            return true;
        }
    }else{
        return true;
    }
}


void DFS(std::map< std::string,std::list<std::string> >& G,const std::string& start, std::list<std::string>& path){
    path.push_back(start);
    if(start == "end"){
        paths.push_back(path);
        ++totalPathCounter;
        return;
    }
    for(const auto& nextCave : G[start]){
        if(isSmallCave(nextCave) && (std::find(path.begin(),path.end(),nextCave)!=path.end())){
            continue;
        }
        DFS(G,nextCave,path);
        path.pop_back();
    }

}



int main(){
    std::map < std::string, std::list<std::string> > graph;
    std::list< std::string > path;
    parse(graph);
    prepareForDFS(graph);
    for(auto vert : graph){
        std::cout << vert.first << " :" ; 
        for(auto x : vert.second){
            std::cout << " " << x ;
        }
        std::cout /*<< " Size : " << vert.second.size()*/ << std::endl;
    }
    
    DFS(graph,START,path);
    
    for(auto p : paths){
        std::cout << " Path : " ;
        for (auto v : p){
            std::cout << " " << v ;
        }
        std::cout << std::endl;
    }
    std::cout << "Size is " << paths.size() << std::endl;
    std::cout << "Size is " << totalPathCounter << std::endl;
}