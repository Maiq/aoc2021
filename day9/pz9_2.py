f=open("in9.txt",'r')

matrix = []
basinList = []
def height(y,x):
    return matrix[y][x]

def flood(coords): 
    y,x = coords
    thisHeight = height(y,x)
    matrix[y][x]
    if (height(y+1,x) > thisHeight and height(y+1,x) < 9):
        flood((y+1,x))
    if (height(y,x+1) > thisHeight and height(y,x+1) < 9):
        flood((y,x+1))
    if (height(y-1,x) > thisHeight and height(y-1,x) < 9):
        flood((y-1,x))
    if (height(y,x-1) > thisHeight and height(y,x-1) < 9):
        flood((y,x-1))
    basinMap.update({coords:1})

for line in f:
    row = []
    line = line.replace('\n','')
    line = list (map(int,line))
    row.extend(line)
    row.insert(0,10)
    row.append(10)
    matrix.append(row)

matrix.insert(0,[10]*len(row))
matrix.append([10]*len(row))


lowPoints = []
basinCenter = []
for y in range(1,len(matrix)-1):
    for x in range(1,len(row)-1):
        cp = matrix[y][x]
        top = matrix[y-1][x]
        bot = matrix[y+1][x]
        left = matrix[y][x-1]
        right= matrix[y][x+1]
        
        if top == cp:
            top -= 1
        if bot == cp:
            bot -= 1
        if left == cp:
            left -= 1
        if right == cp:
            right -= 1
        window = [top,bot,left,right,cp]
        
        if cp == min(window):
            lowPoints.append(cp)
            basinCenter.append((y,x))
        

risklevel = [x+1 for x in lowPoints]
print ("Risk level %d "%sum(risklevel))

#PART 2
# print(basinCenter)
basinMap = {}
basinSize = [0,0,0]
for deepest in basinCenter:
    basinMap.clear()
    flood(deepest)
    # print(len(basinMap))
    basinSize.append(len(basinMap))

basinSize.sort(reverse=True)
answer = basinSize[0]*basinSize[1]*basinSize[2]
print("Basin size product %d" % answer)