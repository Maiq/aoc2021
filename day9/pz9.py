f=open("in9.txt",'r')
matrix = []
for line in f:
    row = []
    line = line.replace('\n','')
    line = list (map(int,line))
    row.extend(line)
    row.insert(0,10)
    row.append(10)
    matrix.append(row)

matrix.insert(0,[10]*len(row))
matrix.append([10]*len(row))

for m in matrix:
    print(m)

lowPoints = []

for y in range(1,len(matrix)-1):
    for x in range(1,len(row)-1):
        cp = matrix[y][x]
        top = matrix[y-1][x]
        bot = matrix[y+1][x]
        left = matrix[y][x-1]
        right= matrix[y][x+1]
        
        if top == cp:
            top -= 1
        if bot == cp:
            bot -= 1
        if left == cp:
            left -= 1
        if right == cp:
            right -= 1
        window = [top,bot,left,right,cp]
        
        if cp == min(window):
            lowPoints.append(cp)
        
print(lowPoints)
risklevel = [x+1 for x in lowPoints]
print (sum(risklevel))