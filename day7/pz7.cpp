#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <cstdint>
#include <cmath>

int main(){
    std::string fileName("in7.txt");
    std::fstream file;
    std::string number;
    std::map<int,int> crabs;

    int maxPos = -1;
    int position = 0;
    int fuel = 0;
    file.open(fileName);
    while(!file.eof()){
        std::getline(file,number,',');
        position = std::stoi(number);
        if(position > maxPos){
            maxPos = position;
        }
        crabs[position] += 1;
    }
    for (auto pair : crabs){
        std::cout << "There are " << pair.second << " crabs on position " << pair.first << std::endl;
    }
    std::cout << "Max position is " << maxPos << std::endl;

    int minFuel = INT32_MAX;
    int destination = -1;

    for(int moveTo = 0 ; moveTo <= maxPos ; ++moveTo){
        int fuelConsumption = 0;
        for(auto pair : crabs){
            fuelConsumption += abs(moveTo - pair.first) * pair.second;
        }
        if( fuelConsumption < minFuel){
            minFuel = fuelConsumption;
            destination = moveTo;
        }
    }
    std::cout << "Min fuel consumption " << minFuel << " after setting at pos " << destination;

}