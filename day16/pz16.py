from math import prod
versionSum = 0
def convertStringToBinString(s):
    binString =  str(bin(int(s,16)))[2:]
    modulo = len(binString)%4
    if modulo == 1:
        padding = "000"
    elif modulo == 2:
        padding = "00"
    elif modulo == 3:
        padding = "0"
    else:
        padding = ""
    
    return padding+binString

def printAsDecimal(s):
    print(int(s,2))

def parseBITS(binaryString):
    global versionSum
    #print("PACKET %s"%binaryString)
    version = binaryString[0:3]
    versionAsInt = int(version,2)
    id = binaryString[3:6]
    ltid = '-1'
    packetValue = 0
    values = []
    effectivePacketSize = 6 # default overhead 
    if ( int(id,2) == 4 ): #literal value
        processedBits,packetValue = parseLiteral(binaryString[6:])
        effectivePacketSize += processedBits
    else: #parse operator
        ltid = binaryString[6]
        effectivePacketSize += 1
        if( ltid == '0' ):
            decimalLength = int(binaryString[7:22],2)# TOTAL BIT LENGTH
            subPackets = binaryString[22:]
            effectivePacketSize += 15
            totalBitLength = 0
            while True:
                trimPacket,v = parseBITS(subPackets)
                totalBitLength += trimPacket
                effectivePacketSize+=trimPacket
                values.append(v)
               # print("Trim packet by %d"%trimPacket)
                subPackets = subPackets[trimPacket:]
                if totalBitLength == decimalLength:
                    break

        if( ltid == '1'): #NUMBER OF PACKETS
            decimalCount = int(binaryString[7:18],2)# PACKET COUNT
            effectivePacketSize += 11
            subPackets = binaryString[18:]
            for i in range(0,decimalCount):
                trimPacket,v = parseBITS(subPackets)
                effectivePacketSize += trimPacket
                subPackets = subPackets[trimPacket:]
                values.append(v)

    if(len(values)>0):
        if int(id,2) == 0:
            #sum
            packetValue = sum(values)
        elif int(id,2) == 1:
            #product
            packetValue = prod(values)
        elif int(id,2) == 2:
            #minumum
            packetValue = min(values)
        elif int(id,2) == 3:
            #maximum
            packetValue = max(values)
        elif int(id,2) == 5:
            #greater than
            packetValue = 1 if values[0] > values[1] else 0
        elif int(id,2) == 6:
            #less than
            packetValue = 1 if values[0] < values[1] else 0
        elif int(id,2) == 7:
            #equal to
            packetValue = 1 if values[0] == values[1] else 0

    versionSum += versionAsInt
    return effectivePacketSize,packetValue

def parseLiteral(binaryString):
    x = 0
    buff = binaryString[x:x+5]
    value = buff[1:]
    x += 5
    while buff[0] != '0' :
        buff = binaryString[x:x+5]
        value += buff[1:]
        x += 5
    # printAsDecimal(value)
    unused = len(binaryString) - x 
    if unused != 0:
        # print("UNUSED!")
        pass
    integerValue = int(value,2)
    return x,integerValue # actual lentgh of processed string


with open("in16.txt") as f:
    data = f.readline().strip()

versionSum = 0
dataBinary = convertStringToBinString(data)
size, value = parseBITS(dataBinary)
print("Sum version %d"%versionSum)
print("Value %d"%value)
