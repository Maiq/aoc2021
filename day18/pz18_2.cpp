#include <algorithm>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <cctype>
#include <stack>
#include <memory>
#include <cmath>

constexpr auto DEPTH = 3;
char name ='A';
class SFNBase: public std::enable_shared_from_this<SFNBase>{
   
    public:
    SFNBase():m_depth(0),m_value(0){};
    explicit SFNBase(const int depth,const int value):m_depth(depth),m_value(value),m_leftNode(nullptr),m_rightNode(nullptr),m_name(name++){};
    virtual int getValue(){
        return m_value;
    }
    int getDepth(){
        return m_depth;
    }
    virtual void setDepth(int depth){
        m_depth = depth;
    }

    virtual std::shared_ptr<SFNBase> getLeftNode(){
        return m_leftNode;
    }
    
    virtual void setLeftNode(std::shared_ptr<SFNBase> leftNode){
        m_leftNode = leftNode;
    }

    virtual std::shared_ptr<SFNBase> getRightNode(){
        return m_rightNode;
    }

    virtual void setRightNode(std::shared_ptr<SFNBase> rightNode){
        m_rightNode = rightNode;
    }
    virtual void addNumber(int number){
        m_value += number;
    }

    virtual void print(){
        //std::cout << m_depth << ":" << getValue();
        // //std::cout << getValue();
    }
    virtual void bury(){
        ++m_depth;
    }

    virtual std::shared_ptr<SFNBase> split(bool& token);

    virtual std::shared_ptr<SFNBase> getRegural(int pairElement){
        return shared_from_this();
    }

    virtual void reduce(){

    };

    virtual std::shared_ptr<SFNBase> explode(bool& token){
        return shared_from_this();
    }
    
    bool isFinal(){
        return true;
    }
    char getName(){
        return m_name;
    }

    virtual void neighbour(){
        int L = -1;
        int R = -1;
        if (getLeftNode() != nullptr){
            L = getLeftNode()->getValue();
        }
        if (getRightNode() != nullptr){
            R = getRightNode()->getValue();
        }

        //std::cout << " " << L << " --" << getName()<<"_"<< getDepth() <<":" <<getValue() <<"-- " << R << std::endl;
    }

    virtual void reconstruct(int depth){
        setDepth(depth-1); //Why -1?
    }

    virtual int magnitude(){
        return m_value;
    }

    private:
    int m_depth;
    int m_value; 
    char m_name;
    
    std::shared_ptr<SFNBase> m_leftNode;
    std::shared_ptr<SFNBase> m_rightNode;
};

class SFN: public SFNBase{
    public:
    SFN(const int depth, std::shared_ptr<SFNBase> A, std::shared_ptr<SFNBase> B):m_pair(A,B),SFNBase(-1,-1){
        setDepth(depth);
        //std::cout << "Creating with depth "<< depth << " setted " << getDepth() << std::endl;
        // //std::cout << "COMBINE ";
        // m_pair.first.get()->print();
        // //std::cout << " " ;
        // m_pair.second.get()->print();
        // //std::cout << std::endl;

        getRightNode().get()->setLeftNode(getLeftNode());
        getLeftNode().get()->setRightNode(getRightNode());
        
       //std::cout << "Wartosc na lewo od " << getName() << getLeftNode().get()->getValue() <<  " na prawo " << getRightNode().get()->getValue() << std::endl;
    };

    std::shared_ptr<SFNBase> getLeftNode(){
        return m_pair.first.get()->getRegural(1);
    }

    std::shared_ptr<SFNBase> getRightNode(){
        return m_pair.second.get()->getRegural(0);
    }

    int getValue(){
        return -1;
    }

    std::shared_ptr<SFNBase> split(bool& token){
        auto verif = m_pair.first->split(token);
        if( verif.get() != m_pair.first.get() ){
            //std::cout << "My value just splited!";
            m_pair.first = verif;
            return shared_from_this();
        }
        if(token){
            return shared_from_this();
        }
        verif = m_pair.second->split(token);
        if(verif.get() != m_pair.second.get() ){
            //std::cout << "My value just splited!";
            m_pair.second = verif;
            return shared_from_this();
        }

        //RIGHT NODE
    
        return shared_from_this();
    }
    




    virtual std::shared_ptr<SFNBase> getRegural(int pairElement) override{
        switch(pairElement){
            case 0:
                return m_pair.first.get()->getRegural(0);                
            case 1:
                return m_pair.second.get()->getRegural(1);
            default:
                //std::cout <<" PROBLEM " << std::endl;
                return nullptr;
        }
    }


    void print() override{
        //std::cout << "_"<<getDepth() <<"_" <<  "[" ;
        m_pair.first.get()->print();
        //std::cout << "," ;
        m_pair.second.get()->print();
        //std::cout << "]" ;
    }
  
    void bury() override{
        
        m_pair.first.get()->bury();
        m_pair.second.get()->bury();
    }
    virtual bool isFinal(){
        return false;
    }
    
    std::shared_ptr<SFNBase> explode(bool& token) override {    
        //std::cout<< " Current depth " << getDepth() << std::endl; 
        if (getDepth() >= DEPTH && !isFinal()){
            // //std::cout <<std::endl << "EXPLODING !" << std::endl;
            // print();
            std::shared_ptr<SFNBase> blast(new SFNBase(getDepth(), 0));
              
            blast.get()->setLeftNode(m_pair.first.get()->getRegural(0)->getLeftNode());
            blast.get()->setRightNode(m_pair.second.get()->getRegural(1)->getRightNode());

            if(blast.get()->getLeftNode()){
                blast.get()->getLeftNode()->addNumber(m_pair.first.get()->getValue());
                blast.get()->getLeftNode()->setRightNode(blast);
            }
            if(blast.get()->getRightNode()){
                blast.get()->getRightNode()->addNumber(m_pair.second.get()->getValue());
                blast.get()->getRightNode()->setLeftNode(blast);
            }

            token = true;
            return blast;
        }// END OF RETURN FROM EXPLOSION 
        
        auto verif = m_pair.first.get()->explode(token);
        if (verif.get() != m_pair.first.get()){
            //std::cout << "My Value just exploded!" ;
            m_pair.first.get()->print();
            //std::cout << std::endl;
            m_pair.first = verif;    
            return shared_from_this();
        }
        if(token){
            return shared_from_this();
        }
        ///RIGHT NODE
        verif = m_pair.second.get()->explode(token);
        if (verif.get() != m_pair.second.get()){
            //std::cout << "My Value just exploded!" ;
            m_pair.second.get()->print();
            //std::cout << std::endl;
            m_pair.second = verif;    
            return shared_from_this();
        }

        return shared_from_this();
    }

    void reduce(){
        bool token = true;  
        print();      
        while(token){
            //std::cout << std::endl << "Reduce" << std::endl;
            token = false;
            print();
            //std::cout <<std::endl;
            neighbour();
            explode(token);
            if(token){
                continue;
            }
            split(token);
            if(token){
                continue;
            }
        }
    }
    void reconstruct(int depth){
        setDepth(depth);
        m_pair.first.get()->reconstruct(getDepth()+1);
        m_pair.second.get()->reconstruct(getDepth()+1);
    }

    int magnitude(){
        int a = m_pair.first.get()->magnitude();
        int b = m_pair.second.get()->magnitude();
        return (3*a + 2*b);
    }

    virtual void neighbour(){
        m_pair.first.get()->neighbour();
        m_pair.second.get()->neighbour();
    }


    private:
    std::pair<std::shared_ptr<SFNBase>,std::shared_ptr<SFNBase > > m_pair;
    
};

//HAXING 
std::shared_ptr<SFNBase> SFNBase::split(bool& token){
    if(m_value >= 10){
        int a = static_cast<int>(floor(static_cast<double>(m_value)/2));
        int b = static_cast<int>(ceil(static_cast<double>(m_value)/2));
        //std::cout << "SPLIT ! " << getValue() << "into " << a << " " << b << std::endl;
        
        std::shared_ptr<SFNBase>A(new SFNBase(m_depth+1, a));
        std::shared_ptr<SFNBase>B(new SFNBase(m_depth+1, b));

        if( getLeftNode() ){
            getLeftNode()->setRightNode(A);
            A.get()->setLeftNode(getLeftNode());
        }
        if( getRightNode() ){
            getRightNode()->setLeftNode(B);
            B.get()->setRightNode(getRightNode());
        }
        
        std::shared_ptr<SFNBase> splinter(new SFN(m_depth,A,B));

        A.get()->neighbour();
        B.get()->neighbour();

        token = true;
        return splinter;
    }
    return shared_from_this();
}



std::shared_ptr<SFNBase> add(std::shared_ptr<SFNBase>& A, std::shared_ptr<SFNBase>& B){
    
    A.get()->bury();
    B.get()->bury();    
    std::shared_ptr<SFNBase> output(new SFN(0,A,B));
    output.get()->reduce();
    output.get()->reconstruct(0);
    return output;
}
std::vector<std::shared_ptr<SFNBase> > parse(){
    const std::string fileName("in18.txt");
    std::fstream file;
    std::string buffLine;
    file.open(fileName);
    std::vector<std::shared_ptr<SFNBase> > input;
    while(!file.eof()){
        std::getline(file,buffLine);
        std::stack<std::shared_ptr<SFNBase> > _stack;
        int depth = -1;
        for(auto _char : buffLine){
            if (_char == '['){
                ++depth;
            }
            else if( _char == ']'){
                auto second(_stack.top());
                _stack.pop();

                auto first(_stack.top());
                _stack.pop();

                _stack.emplace(std::shared_ptr<SFNBase>(new SFN(depth,first,second)));
                --depth;

            }
            else if( std::isdigit(_char) ){
                int val = _char - '0';
                _stack.emplace(std::shared_ptr<SFNBase>( new SFNBase(depth,val)));

            }
        }
        if(!_stack.empty()){
            // _stack.top().get()->print();
            input.push_back(_stack.top());
        }
        //std::cout << std::endl;
      
        while (!_stack.empty()){
            _stack.pop();
        }
    }
    file.close();
    return input;
}

int main(){
    std::vector<std::shared_ptr<SFNBase> > input;
    std::stack<std::shared_ptr<SFNBase> > solution;
    input = parse();

    // input[0].get()->neighbour();

    // solution.push(input[0]);
    // char c;
    // int i = 0;
    // for (i = 1; i < input.size() ; ++i){
    //     //std::cout << "New loop" << std::endl;
    //     auto rev = solution.top();
    //     solution.pop();
    //     auto x = add(rev,input[i]);
    //     solution.push(x);
    //     solution.top().get()->print();
    // }
    auto max = 0;
    for (int i = 0 ; i < input.size() - 1 ;++i){
        input = parse();
        for (int j = i+1; j < input.size() ; ++j){
            input = parse();
            auto x = add(input[i],input[j]);
            auto magnitude = x.get()->magnitude();
            if( magnitude > max ){
                max = magnitude;
            }

        }
    }
    for ( int i = input.size()-1 ; i > 0 ; --i ){
        input = parse();
        for (int j = i-1; j >= 0 ; --j){
            input = parse();
            auto x = add(input[i],input[j]);
            auto magnitude = x.get()->magnitude();
            if( magnitude > max ){
                max = magnitude;
            }

        }

    }




    // auto x = add(input[8],input[0]);
    std::cout << "Magnitude is sum " << max << std::endl;
 

    // //std::cout << std::endl <<"Magnigute " << solution.top().get()->magnitude() << std::endl;

    return 0;    
}