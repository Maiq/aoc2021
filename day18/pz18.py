from math import floor, ceil

class SFN():
    def __init__(self,depth,number,lNode = None, rNode = None):
        self.depth = depth
        self.value = number
        self.lNode = lNode
        self.rNode = rNode

    def __repr__(self):
        # return "<SFN(%d) d:%s %s l:%d r:%d>" % (id(self),self.depth , self.value, id(self.lNode),id(self.rNode))
        return "d_%s:%s"%(self.depth,self.value)

    def lNodeSet(self, other):
        self.lNode = other
    
    def rNodeSet(self, other):
        self.rNode = other

    def lastRightNode(self):
        for n in self.value:
            if isinstance(n,SFN):
                if n.rNode is None:
                    print("Empty R-Node on %s"%id(n))
                    return n
                else:
                    n.lastRightNode()
        return self

    def lastLeftNode(self):
        for n in self.value:
            if isinstance(n,SFN):
                if n.lNode is None:
                    print("Empty L-Node on %s"%id(n))
                    return n
                else:
                    n.lastLeftNode()
        return self

    def bury(self):
        self.depth += 1
        for n in self.value:
            if isinstance(n,SFN):
                n.bury()

    def __add__(self, other):        
        #self.value[-1].rNode = other.value[0]
        #other.value[0].lNode = self.value[-1]
        _selfLastRightNode = self.lastRightNode()
        _otherLastLeftNode = other.lastLeftNode()
        # print("Rajt node %s"%_selfLastRightNode)
        # print("left node %s"%_otherLastLeftNode)
        _selfLastRightNode.rNodeSet( _otherLastLeftNode)
        _otherLastLeftNode.lNodeSet(_selfLastRightNode)

        self.bury()
        other.bury()
        
        sum = SFN(0,[self,other])
        # sum.reduce()
        return sum

    def reduce(self):
        exploded = True
        splited = True
        while( exploded or splited ):
            print("Time to explode")
            exploded = self.explode()
            print("Time to split")
            splited = self.split()
            print("After reduce loop%s"%self)
        return self

    def explode(self):
        # input()
        # print(self)
        exploded = True
        if self.depth < 5:
            while exploded:
                exploded = False
                for n in self.value:
                    if isinstance(n,SFN):
                        if n.explode():
                            self.value.remove(n)
                            exploded = True
                            break
        elif self.depth >= 5:
            print("Moja zawartosc %s"%self.value)
            if self.lNode:
                print("Dodaje do lewego %d"%self.lNode.value[-1])
                self.lNode.rNodeSet(self.rNode)
                self.lNode.value[-1] += self.value[0]
            else:
                print("nic nie zrobie z wartoscia%d"%self.value[0])
            if self.rNode:
                print("Dodaje do prawego %d"%self.rNode.value[0])
                self.rNode.lNodeSet(self.lNode)
                self.rNode.value[0] += self.value[1]
            else:
                print("Nic nie zrobie z wartoscia%d"%self.value[-1])
            return True
        
        return False

    def split(self):
        n = 0 
        print(self)
        input()
        for number in self.value:
            if not isinstance(number, SFN):
                print("Not an instcance")
                print(number)
                continue
            elif len(number.value) == 1:
                val = number.value[0]
                if not isinstance(val,SFN):
                    if val >= 10:
                        new = SFN(number.depth+1, [floor(val/2),ceil(val/2)])
                        print ("Split this%s"%number)
                        print ("Create this%s"%new)
                        if number.lNode:
                            number.lNode.rNodeSet(new)
                            new.rNodeSet(number.lNode)
                        if number.rNode:
                            number.rNode.lNodeSet(new)
                            new.lNodeSet(number.rNode)
                        self.value[n] = new
                        return True
            else:
                if number.split():
                    return True
            n += 1
        return False


def parse():
    with open("ex18.txt") as f:
        parsedList = []
        for line in f.readlines():
            depth = 0
            val = []
            sfnList = []
            for char in line.strip():
                if char == '[':
                    if val:
                        sfnList.append(SFN(depth,val.copy()))
                        val.clear()
                    depth += 1
                if char == ']':
                    if val:
                        sfnList.append(SFN(depth,val.copy()))
                        val.clear()
                    depth -= 1
                if char.isdigit():
                    val.append(int(char))
            
            for i in range(0, len(sfnList) - 1):
                sfnList[i].rNode = sfnList[i+1] 
                sfnList[i+1].lNode = sfnList[i]

            parsedList.append(SFN(0,sfnList.copy()))
    return parsedList

homework = parse()
# s1 = homework[0]
# s2 = homework[1]

# print(s1)
# print(s2)

# x = s1 + s2

a1 = homework[-2]
a2 = homework[-1]
x = a1 + a2
print(x)
x.reduce()
