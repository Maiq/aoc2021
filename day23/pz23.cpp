#include <iostream>
#include <algorithm>
#include <array>
#include <stack>
#include <vector>

static constexpr auto ROOMSIZE = 2;
static constexpr auto ROOMCOUNT = 4;

char decodeSymbol(int h){
    char symbol;
    switch(h){
                case -1:{
                    //fallthrough
                }
                case 0:{
                    symbol = '.';
                    break;
                }
                case 1:{
                    symbol = 'A';
                    break;
                }
                case 10:{
                    symbol = 'B';
                    break;
                }
                case 100:{
                    symbol = 'C';
                    break;
                }
                case 1000:{
                    symbol = 'D';
                    break;
                }
                default:{
                    symbol = '@';
                    break;
                }
            }
    return symbol;
}

class State{
    public:
    State(std::array<std::stack<int>,4> R,
          std::array<int,11> H,
          int E):rooms(R),hallway(H),energyCounter(E){};

    void print(){
        std::array<std::stack<int>,4> tmp;
        char symbol;
        std::cout << "#############" << std::endl;
        printHallway(hallway);
        std::cout << "###";
        tmp = rooms;
        for(auto i = 0 ; i < tmp.size(); ++i){
            if(tmp[i].size() == ROOMSIZE){
                symbol = decodeSymbol(tmp[i].top());
                tmp[i].pop();
            }
            else{
                symbol = '.';
            }
            std::cout << symbol << "#";
        }
        std::cout << "##" << std::endl;
        std::cout << "  #";
        for(auto i = 0 ; i < tmp.size(); ++i){
            if(tmp[i].size() != 0){
                symbol = decodeSymbol(tmp[i].top());
            }
            else{
                symbol = '.';
            }
            std::cout << symbol << "#";
        }
        std::cout << "  " << std::endl;
        std::cout << "  #########  : "<< energyCounter << std::endl;

    }
    void printHallway(std::array<int,11> H){
        std::cout <<"#";
        for(auto h : H){
            char symbol;
            symbol = decodeSymbol(h);
            std::cout<< symbol;
        }
        std::cout << "#" << std::endl;
    }

    std::vector<State> evaluate(int room){
        std::vector<State> evaluated;
        if(room == ROOMCOUNT){// EVALUATE HALLWAY
            for(int i = 0; i < hallway.size(); ++i){
                if( hallway[i] > 0){
                    //evaluate from left to right;
                    int dest = hallway[i]/10; // check if given room has place;
                    int log = 0;
                    while(dest !=0 ){
                        dest /= 10;
                        ++log;
                    }
                    if(rooms[log].size() < ROOMSIZE){ // OK we can try to go there;
                        int destIdx = (2*log) + 2; // check which hallway index is on entrance to room

                        int move = 0;
                        if(destIdx < i){ // we have to go left;
                            move = -1;
                        }
                        else{ //we have to go right;
                            move = 1;
                        }
                        int pos = i;
                        bool obstacle = false;
                        while(pos != destIdx){
                            pos += move;
                            if(hallway[pos] > 0){//we can't move!
                                obstacle = true;
                                // break; 
                            }
                        }
                        if(obstacle){
                            continue; // Can't evaluate there's obstacle
                        }
                        //Finnally push amphiod into room and create new state;
                        auto newHall = hallway;
                        newHall[i] = 0;
                        auto newRooms = rooms;
                        newRooms[log].push(hallway[i]);
                        auto newEnergy = energyCounter;
                        int steps = std::abs(i-destIdx);
                        newEnergy += (hallway[i]*steps);
                        newEnergy += (hallway[i]*(ROOMSIZE-rooms[log].size()));
                        evaluated.emplace_back(State(newRooms,newHall,newEnergy));
                    }
                }
            }
        }
        else{ //EVALUATE ROOMS;
            if(rooms[room].size()>0){//Check if anything in this room;
                int destIdx = (2*room) + 2; //Create hallway room index
                
                //EVALUATE TO LEFT;
                int pos = destIdx;

                // lambda for creating pop amphipod from room and left him here;
                auto lambdaState = [&]()
                {
                    auto newRooms = rooms;
                    auto newHallValue = newRooms[room].top();
                    newRooms[room].pop();
                    auto newHall = hallway;
                    newHall[pos] = newHallValue;
                    auto newEnergy = energyCounter;
                    int steps = std::abs(destIdx-pos);
                    newEnergy += newHallValue*steps;
                    newEnergy += newHallValue*(ROOMSIZE-rooms[room].size() + 1);
                    evaluated.emplace_back(State(newRooms,newHall,newEnergy));
                };

                while( pos >= 0 ){
                    if(hallway[pos] == 0){ //Ok we can move here;
                        lambdaState();
                    }
                    else{
                        if(hallway[pos] > 0 ){
                            //THERE'S obstacle, cant move more left;
                            break;
                        }
                    }
                    pos -= 1;
                }
                //EVALUATE TO RIGHT;
                pos = destIdx;
                while( pos < hallway.size() ){
                    if(hallway[pos] == 0){ //Ok we can move here;
                        lambdaState();  
                    }
                    else{
                        if(hallway[pos] > 0 ){
                            //THERE'S obstacle, cant move more left;
                            break;
                        }
                    }
                    pos += 1;
                }
            }
        }
        return evaluated;
    }

    private:
    std::array< std::stack<int>,4> rooms;
    std::array<int,11> hallway = {0};
    int energyCounter;
};


int main(){
    std::array<std::stack<int>,4> R;
    std::array<int,8> tmp = {1,10,1000,100,100,10,1,1000};
    int t = 0;
    for(auto& r :R){
        r.push(tmp[t++]);
        r.push(tmp[t++]);
    }
    std::array<int,11> H = {0,0,-1,0,-1,0,-1,0,-1,0,0};
    
    State S(R,H,0);
    S.print();
    std::cout << std::endl;
    auto V = S.evaluate(2);
    for(auto& v : V){
        v.print();
        std::cout << std::endl;
    }
    auto V2 = V[1].evaluate(1);
    for(auto& v : V2){
        v.print();
        std::cout << std::endl;
    }

    auto V3 = V2[0].evaluate(4);
    for(auto& v : V3){
        v.print();
        std::cout << std::endl;
    }

    auto V4 = V3[1].evaluate(1);
    for(auto& v : V4){
        v.print();
        std::cout << std::endl;
    }

    auto V5 = V4[0].evaluate(4);
    for(auto& v : V5){
        v.print();
        std::cout << std::endl;
    }

    auto V6 = V5[0].evaluate(3);
    for(auto& v : V6){
        v.print();
        std::cout << std::endl;
    }

    auto V7 = V6[0].evaluate(3);
    for(auto& v : V7){
        v.print();
        std::cout << std::endl;
    }

    auto V8 = V7[0].evaluate(4);
    for(auto& v : V8){
        v.print();
        std::cout << std::endl;
    }

}