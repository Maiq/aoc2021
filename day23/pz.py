#!/usr/bin/env python3

A = 1
B = 10
C = 100
D = 1000
w = -1
d = -2
o = 0 

AMP_LIST = [A,B,C,D]

CHAR_MAP = {A:'A',B:'B',C:'C',D:'D',w:'#',o:'.',d:'.'}

DOORS = [(0,2),(0,4),(0,6),(0,8)]

INVALID = (-1,-1,-1,-1,-1)

# S = [ [o,o,d,o,d,o,d,o,d,o,o],
#       [w,w,B,w,C,w,B,w,D,w,w],
#       [w,w,A,w,D,w,C,w,A,w,w] ]

S = [ [o,o,d,o,d,w,d,w,d,w,w],
      [w,w,B,w,A,w,w,w,w,w,w],
      [w,w,A,w,B,w,w,w,w,w,w] ]

least_energy = 100000000000000

def print_map(diag):
    for l in diag:
        print(''.join([CHAR_MAP[x] for x in l]))


def get_goal(x):
    goals = {A:2,B:4,C:6,D:8}
    return goals[x]


def vacant(p):
    return p == d or p == o


def room_ready(G,a):
    g = get_goal(a)
    if G[1][g] == o and (G[2][g] == a or G[2][g] == o):
       return True
    return False 


def start_from_hallway(yo):
    return yo == 0


def end_on_doors(y,x):
    return (y,x) in DOORS


def already_in_room(a,xo):
    return get_goal(a) == xo


def get_empty_tiles(G):
    available = []
    for y in range(3):
        available.extend([(y,x) for x in range(11) if G[y][x] == o])
    return available


def get_amphiod_tiles(G):
    amps = []
    for y in range(3):
        amps.extend([(y,x) for x in range(11) if G[y][x] in AMP_LIST])
    return amps


def move(G,yo,xo,y,x):
    a = G[yo][xo]
    e = 0
    h = 0
    g = get_goal(a)
    if already_in_room(a,xo):
        return INVALID

    if start_from_hallway(yo): #check if the move from hallway is valid
        if not room_ready(G,a):
            return INVALID

    if end_on_doors(y,x):
        return INVALID
    
    for yi in range(yo-1,-1,-1): #move up
        if not vacant(G[yi][xo]):
            return INVALID
    e = a*(abs(yo-0))
    
    move_range = range(x+1,xo+1) if x <xo else range(xo+1,x+1)
    for xi in move_range:
        if not vacant(G[0][xi]):
            return INVALID
    
    e += a*(abs(xo-x))
    h = a*(abs(x-g))
    
    for yi in range(y+1):
        if not vacant(G[yi][x]):
            return INVALID
    e += a*(abs(0-y))
    return (e,h,yo,xo,y,x)


def sort_to_best(org_cost):
    cost = list(filter(lambda x: x != INVALID, org_cost))
    cost.sort(key=lambda x: (x[1],x[0]))
    if len(cost) <= 1: # At least 1 element have to be
        return cost
    x = cost[1] #Sort to go deepest
    if cost[1][1] == 0:
        cost[1] = cost[0]
        cost[0] = x
    return cost


def swap_elements(org_G,move_output):
    G = org_G
    e,h,yo,xo,y,x = move_output
    tmp = G[y][x]
    G[y][x] = G[yo][xo]
    G[yo][xo] = tmp
    return G


def solve(G,energy):
    print("_______\n")
    print_map(G)
    moves = []
    _energy = energy
    for y,x in get_amphiod_tiles(G):
        moves.extend([move(G,y,x,dy,dx) for dy, dx in get_empty_tiles(G)])
    sorted_moves = sort_to_best(moves)
    print(sorted_moves)
    for mv in sorted_moves:
        print(f'Solve for move from {mv[2],mv[3]} to {mv[4],mv[5]}')
        new_G = swap_elements(G,mv)
        solve(new_G, _energy)
    


if __name__ == '__main__':
    solve(S,0)