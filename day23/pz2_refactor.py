#!/usr/bin/env python3

A = 1
B = 10
C = 100
D = 1000
w = -1
d = -2
o = 0 

AMP_LIST = [A,B,C,D]

CHAR_MAP = {A:'A',B:'B',C:'C',D:'D',w:'#',o:'.',d:'.'}

DOORS = [(0,2),(0,4),(0,6),(0,8)]

INVALID = (-1,-1,-1,-1,-1)

DEPTH = 3

S = [ [o,o,d,o,d,o,d,o,d,o,o],
      [w,w,B,w,C,w,B,w,D,w,w],
      [w,w,A,w,D,w,C,w,A,w,w] ]

S_2 = [ [o,o,d,o,d,o,d,o,d,o,o],
        [w,w,B,w,C,w,B,w,D,w,w],
        [w,w,D,w,C,w,B,w,A,w,w],
        [w,w,D,w,B,w,A,w,C,w,w],
        [w,w,A,w,D,w,C,w,A,w,w] ]

# S = [ [o,o,d,D,d,o,d,A,d,o,o],
#       [w,w,o,w,B,w,C,w,o,w,w],
#       [w,w,A,w,B,w,C,w,D,w,w] ]

S_moje = [ [o,o,d,o,d,o,d,o,d,o,o],
      [w,w,A,w,D,w,A,w,B,w,w],
      [w,w,C,w,C,w,D,w,B,w,w] ]

S_moje2 = [ [o,o,d,o,d,o,d,o,d,o,o],
            [w,w,A,w,D,w,A,w,B,w,w],
            [w,w,D,w,C,w,B,w,A,w,w],
            [w,w,D,w,B,w,A,w,C,w,w],
            [w,w,C,w,C,w,D,w,B,w,w] ]

GOAL = [ [o,o,d,o,d,o,d,o,d,o,o],
         [w,w,A,w,B,w,C,w,D,w,w],
         [w,w,A,w,B,w,C,w,D,w,w] ]

GOAL_2 = [ 
         [o,o,d,o,d,o,d,o,d,o,o],
         [w,w,A,w,B,w,C,w,D,w,w],
         [w,w,A,w,B,w,C,w,D,w,w],
         [w,w,A,w,B,w,C,w,D,w,w],
         [w,w,A,w,B,w,C,w,D,w,w] ]

least_energy = 100000000000000

def print_map(diag):
    for l in diag:
        print(''.join([CHAR_MAP[x] for x in l]))


def get_goal(x):
    goals = {A:2,B:4,C:6,D:8}
    return goals[x]


def room_ready(G,a):
    g = get_goal(a)
    global DEPTH
    _f = lambda i: G[i][g] == o or G[i][g] == a
    return all( [ _f(i) for i in range(1,DEPTH) ])

def start_from_hallway(yo):
    return yo == 0


def already_in_room(a,xo):
    return get_goal(a) == xo


def get_empty_tiles(G):
    available = []
    global DEPTH
    for y in range(DEPTH):
        available.extend([(y,x) for x in range(11) if G[y][x] == o])
    return available


def get_amphiod_tiles(G):
    amps = []
    global DEPTH
    for y in range(DEPTH):
        amps.extend([(y,x) for x in range(11) if G[y][x] in AMP_LIST])
    return amps

def moveable(G,y,x):
    # print(f'move to {y,x}')
    if G[y][x] != o and G[y][x] != d:
            return False
    return True

def move(G,yo,xo,y,x):
    e = 0
    h = 0
    a = G[yo][xo]
    g = get_goal(a)
    for yi in range(yo-1,-1,-1):
        if not moveable(G,yi,xo):
            return INVALID
        e += a
    xrg = (xo-1,x-1,-1) if x < xo else (xo+1,x+1,1)
    for xi in range(xrg[0],xrg[1],xrg[2]):
        if not moveable(G,0,xi):
            return INVALID
        e += a
    for yi in range(1,y+1,1):
        if not moveable(G,yi,x):
            return INVALID
        e += a
    h = abs(xi-g)
    # print(f'Move From: {yo,xo} -> {y,x}: ({e,h})')
    return (e,h,yo,xo,y,x)


def sort_to_best(org_cost):
    cost = list(filter(lambda x: x != INVALID, org_cost))
    cost.sort(key=lambda x: (x[1],x[0]))
    if len(cost) <= 1: # At least 1 element have to be
        return cost
    x = cost[1] #Sort to go deepest
    if cost[1][1] == 0:
        cost[1] = cost[0]
        cost[0] = x
    return cost


def evaluate_graph(G,amps, empty):    
    moves = []
    for yo,xo in amps:
        a = G[yo][xo]
        for yd, xd in empty:
            if yo == 0 and yd == 0: #forbid move on hallway.
                continue
            if yd != 0 and xd != get_goal(a): 
                continue
            if yd != 0 and not room_ready(G,a):
                continue
            if xd != get_goal(a) and yd !=0:
                continue
            if xo == get_goal(a) and room_ready(G,a):
               continue
            moves.append(move(G,yo,xo,yd,xd))
    sorted = sort_to_best(moves)
    return sorted

def graph_to_string(G):
    return ''.join([ ''.join(str(row)) for row in G])

BEST_ENERGY = None
VISITED = {}

def solve(G,energy,amps,empty):
    global BEST_ENERGY
    global GOAL
    global GOAL_2
    global DEPTH
    global VISITED

    goal_graph = GOAL_2 if DEPTH == 5 else GOAL
    # print_map(G)
    # print_map(goal_graph)
    strKey = graph_to_string(G)
    if strKey in VISITED:
        if  energy > VISITED[strKey]: # return with been here with lower energy")
            return 
    VISITED[strKey] = energy
    moves = evaluate_graph(G,amps,empty)
    
    if G == goal_graph:
        BEST_ENERGY = energy
        with open('/tmp/best_energies.txt','a+') as f:
            print(f'Finished! {BEST_ENERGY}', file = f)
            print(f'Finished! {BEST_ENERGY}')
    for e,h,yo,xo,y,x in moves:
        new_amp = amps.copy()
        new_amp.remove((yo,xo))
        new_amp.append((y,x))
        new_empty = empty.copy()
        new_empty.remove((y,x))
        new_empty.append((yo,xo))

        G1 = [row.copy() for row in G]
        tmp = G1[yo][xo]
        G1[yo][xo] = G1[y][x]
        G1[y][x] = tmp
        e1 = energy + e
        if BEST_ENERGY != None and e1 > BEST_ENERGY:
            continue
        solve(G1,e1,new_amp,new_empty)

if __name__ == '__main__':
    input_graph = S_moje2
    DEPTH = len(input_graph)
    
    source = get_amphiod_tiles(input_graph)
    dest = get_empty_tiles(input_graph)
    solve(input_graph,0,source,dest)