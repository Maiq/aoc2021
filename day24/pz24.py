#!/usr/bin/env python3

import pprint
from copy import deepcopy
import numpy as np
from functools import lru_cache

def inp(a):
  return a

def add(a,b):
  return a+b

def mul(a,b):
  return a*b

def div(a,b):
  if(b == 0):
    raise ValueError("DZIELENIE PRZEZ ZERO")
  return int(a/b)

def mod(a,b):
  if a < 0 or b<=0:
    raise ValueError(f'Modulo {a} and {b}')
  return a%b

def eql(a,b):
  return 1 if a == b else 0

ALU = {'inp': inp, 'add': add, 'mul':mul, 'mod': mod, 'eql':eql, 'div':div}
REG = {'x':0, 'y':0,'z':0,'w':0}

def eval(cmd,input,reg):
  _REG = deepcopy(reg)
  f, args = cmd[0], cmd[1:]
  if f != 'inp':
    a,b = args[0], args[1]
    if b in _REG:
      _REG[a]= ALU[f](_REG[a],_REG[b])
    else:
      _REG[a]= ALU[f](_REG[a],int(b))
  else:
    a = args[0]
    _REG[a] = ALU[f](int(input))
  return _REG

with open('input.txt','r') as file:
  program = []
  rutine = []
  for line in file.readlines():
    if 'inp' in line and rutine:
      program.append(rutine.copy())
      rutine = []
    rutine.append(line)
  program.append(rutine.copy())


@lru_cache
def branchA(z, w, y): #divide 1
  return z*26 + w + y

@lru_cache
def branchB(z, w, y, x): #divide 26
  if z%26 + x != w:  
    return z + w + y
  else:
    return z//26

new_program = []
for cmd in program:
  new_cmd = []
  for c in cmd:
    x = c.strip().split()
    if len(x) == 3 and (x[-1].isnumeric() or (x[-1][0] == '-' and x[-1][1:].isnumeric())):
      x[-1] = int(x[-1])
    new_cmd.append(x)
  new_program.append(new_cmd)

@lru_cache
def solve(depth,z):
  if depth == 14:
    if z == 0:
      print("DONE!") 
      return True
    return

  for w in range(9,0,-1):
    cmd = new_program[depth]
    if cmd[4][-1] == 1:
      z = branchA(z,w,cmd[-3][-1])
    else:
      z = branchB(z,w,cmd[-3][-1],cmd[5][-1])
    if solve(depth + 1,z):
      print(w)
      return True
       

      
if __name__ == '__main__':
  solve(0,0)
